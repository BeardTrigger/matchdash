﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public enum Player
	{
		None,
		Bottom,
		Top,
	}

	public class TerritoryGrid : Grid
	{
		[Header ("Territory Distributon")]
		[SerializeField]
		[Range (1, 15)]
		protected int playerOneOwnership;
		[SerializeField]
		[Range (1, 15)]
		protected int playerTwoOwnership;

		public float PlayerOneOwnsPercent;
		public float PlayerTwoOwnsPercent;
		public float NoOnesLandPercent;

		protected int playerOneCount;
		protected int playerTwoCount;
		protected int NoOnesCount;

		protected int total;

		protected override void Awake ()
		{
			total = numRows * numCols;
			NoOnesCount = total;
			base.Awake ();
		}

		protected override void Start ()
		{
			base.Start ();
			EnableGrid ();
		}

		protected override void GenerateGrid ()
		{
			base.GenerateGrid ();

			for (int y = 0; y < numRows; y++)
			{
				for (int x = 0; x < numCols; x++)
				{
					var newGridObject = GetGridObject (x, y);
					newGridObject.Initialize (GridObjectType.Territory, Player.None, x, y);
					newGridObject.PlayerChanged += OnOwnerChanged;


					AssignTerritory ();
					if (y < playerOneOwnership)
					{
						newGridObject.SetOwnership (Player.Bottom);
					} else if (y >= (numRows - playerTwoOwnership))
					{
						newGridObject.SetOwnership (Player.Top);
					} else
					{
						newGridObject.SetOwnership (Player.None);
					}

				}
			}
		}

		private void OnOwnerChanged (Player from, Player to)
		{
			switch (from)
			{
				case Player.Bottom:
					playerOneCount--;
					break;
				case Player.Top:
					playerTwoCount--;
					break;
				case Player.None:
					NoOnesCount--;
					break;
			}
			switch (to)
			{
				case Player.Bottom:
					playerOneCount++;
					break;
				case Player.Top:
					playerTwoCount++;
					break;
				case Player.None:
					NoOnesCount++;
					break;
			}

			PlayerOneOwnsPercent =  playerOneCount / (float)total * 100;
			PlayerTwoOwnsPercent = playerTwoCount / (float)total * 100;
			NoOnesLandPercent = NoOnesCount / (float)total * 100;
		}

		protected void AssignTerritory ()
		{
			//TODO: Read From File
		}
	}
}