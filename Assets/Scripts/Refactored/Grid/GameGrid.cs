﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public class GameGrid : Grid
	{
		[Header ("Game Grid Objects")]
		[SerializeField]
		protected GridObject matchableGridObject;
		protected Queue<GridObject> matchableGridObjectQueue;

		protected GridObject EmptyGameGridObject;

		protected Quaternion playerOneRotation;
		protected Quaternion playerTwoRotation;

		protected override void Awake ()
		{
			EmptyGameGridObject = Instantiate (gridObject, transform);
			EmptyGameGridObject.Initialize (GridObjectType.Empty, Player.None, -1, -1);

			playerOneRotation = EmptyGameGridObject.transform.rotation;
			EmptyGameGridObject.transform.Rotate (Vector3.forward, 180);
			playerTwoRotation = EmptyGameGridObject.transform.rotation;

			matchableGridObjectQueue = new Queue<GridObject> ();

			base.Awake ();
		}

		protected override void GenerateGrid ()
		{
			for (int y = 0; y < numRows; y++)
			{
				for (int x = 0; x < numCols; x++)
				{
					var matchable = Instantiate (matchableGridObject, transform).GetComponent<MatchableGridObject> ();
					matchable.MatchableCleared += OnMatchableCleared;
					matchable.DisableGridObject ();
					matchableGridObjectQueue.Enqueue (matchable);

					gridObjectArrays [x, y] = EmptyGameGridObject;
				}
			}
		}

		public GridObject AddMatchableGridObject (GridObjectType gridObjectType, Player player, int x, int y)
		{
			var newMatchableGridObject = matchableGridObjectQueue.Dequeue ();

			if (player == Player.Bottom)
				newMatchableGridObject.transform.rotation = playerOneRotation;
			else
				newMatchableGridObject.transform.rotation = playerTwoRotation;

			newMatchableGridObject.transform.localScale = gridObjectScale;
			newMatchableGridObject.Initialize (gridObjectType, player, x, y);
			newMatchableGridObject.EnableGridObject ();
			SetGridObject (newMatchableGridObject, x, y);
			return newMatchableGridObject;
		}

		public void SetToEmpty(int x, int y)
		{
			SetGridObject (EmptyGameGridObject, x, y);
		}

		protected void OnMatchableCleared(GridObject matchableGridObject)
		{
			matchableGridObjectQueue.Enqueue (matchableGridObject);
		}
		
		public void SubcribeToInteractionReporting(Action<GridObject> onDown, Action<GridObject> onUp, Action<GridObject> onEnter, Action<GridObject> onExit)
		{
			foreach (var gridObj in matchableGridObjectQueue)
			{
				gridObj.InteractionReporter.PointerDown += onDown;
				gridObj.InteractionReporter.PointerUp += onUp;
				gridObj.InteractionReporter.PointerEnter += onEnter;
				gridObj.InteractionReporter.PointerExit += onExit;
			}
		}


		public bool LoopCondition(Direction direction, int index)
		{
			switch (direction)
			{
				case Direction.Horizontal:
					if (index < 0 || index >= numCols)
						return false;
					else
						return true;

				case Direction.Vertical:
					if (index < 0 || index >= numRows)
						return false;
					else
						return true;
			}
			return false;
		}
		public int LoopIncriment (Player player, Direction direction)
		{
			switch (player)
			{
				case Player.Bottom:
					switch (direction)
					{
						case Direction.Up:
						case Direction.Right:
							return 1;
						case Direction.Down:
						case Direction.Left:
							return -1;
					}
					break;
				case Player.Top:
					switch (direction)
					{
						case Direction.Up:
						case Direction.Right:
							return -1;
						case Direction.Down:
						case Direction.Left:
							return 1;
					}
					break;
			}
			return 0;
		}
	}
}
