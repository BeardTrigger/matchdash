﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public class Grid : MonoBehaviour
	{
		[Header("Grid Display Properties")]
		[SerializeField]
		protected int numCols;
		[SerializeField]
		protected int numRows;
		[SerializeField]
		protected float spacing;
		[SerializeField]
		protected float buffer;

		private float screenWidthInWorldSpace;
		private float screenHeightInWorldSpace;
		private float xStep;
		private float yStep;
		private float xStartingPos;
		private float yStartingPos;
		protected Vector3 gridObjectScale;

		[Header ("Grid Object")]
		[SerializeField]
		protected GridObject gridObject;
		protected GridObject [,] gridObjectArrays;

		public int NumCol { get { return numCols; } }
		public int NumRows { get { return numRows; } }

		protected virtual void Awake ()
		{
			gridObjectArrays = new GridObject [numCols, numRows];

			var MinPoint = Camera.main.ViewportToWorldPoint (new Vector3 (0f, 0f, 0f));
			var MaxPoint = Camera.main.ViewportToWorldPoint (new Vector3 (1f, 1f, 1f));

			screenWidthInWorldSpace = MaxPoint.x - MinPoint.x;
			screenHeightInWorldSpace = MaxPoint.y - MinPoint.y;

			xStep = screenWidthInWorldSpace / (float)numCols;
			yStep = (screenHeightInWorldSpace - buffer) / (float)numRows;

			xStartingPos = MinPoint.x;
			yStartingPos = MinPoint.y;

			gridObjectScale = Vector3.one;
			float scalar = xStep - (spacing / 2f);
			gridObjectScale.Set (scalar, scalar, scalar);

			GenerateGrid ();
			FitGrid ();
			DisableGrid ();
		}

		protected virtual void Start ()
		{
		}

		protected virtual void GenerateGrid ()
		{
			for (int y = 0; y < numRows; y++)
			{
				for (int x = 0; x < numCols; x++)
				{
					gridObjectArrays [x, y] = Instantiate (gridObject, transform).GetComponent<GridObject> ();
				}
			}
		}

		protected virtual void FitGrid ()
		{
			for (int y = 0; y < numRows; y++)
			{
				for (int x = 0; x < numCols; x++)
				{
					FitGridObject (x, y);
				}
			}
		}

		public void EnableGrid ()
		{
			foreach (var gridObject in gridObjectArrays)
			{
				gridObject.EnableGridObject ();
			}
		}
		public void DisableGrid ()
		{
			foreach (var gridObject in gridObjectArrays)
			{
				gridObject.DisableGridObject ();
			}
		}
	
		public virtual GridObject GetGridObject ( int x, int y)
		{
			if (gridObjectArrays [x, y] != null)
				return gridObjectArrays [x, y];
			else
				Debug.Break ();

			return null;
		}

		public virtual void SetGridObject(GridObject gridObject, int x, int y)
		{
			gridObjectArrays [x, y] = gridObject;
		}

		public Vector3 GetGridObjectWorldPosition (int x, int y)
		{
			Vector3 pos = Vector3.zero;
			pos.x = ((xStep * (x + 1)) / 2f) + (xStartingPos + ((x * xStep) / 2f));
			pos.y = ((yStep * (y + 1)) / 2f) + (yStartingPos + (buffer / 2f) + ((y * yStep) / 2f));
			return pos;
		}

		protected void FitGridObject(int x, int y)
		{
			gridObjectArrays [x, y].transform.position = GetGridObjectWorldPosition (x, y);
			gridObjectArrays [x, y].transform.localScale = gridObjectScale;
		}		
	}
}
