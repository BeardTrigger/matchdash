﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{

	public class GameGridMatchController : MonoBehaviour {

		[Header ("Grid")]
		[SerializeField]
		protected GameGrid gameGrid;
		[SerializeField]
		protected TerritoryGrid territoryGrid;

		protected List<GridObject> piecesToClear = new List<GridObject> ();
		protected List<GridObject> matchingPieces = new List<GridObject> ();

		protected int XDim;
		protected int YDim;

		protected void Awake ()
		{
			XDim = territoryGrid.NumCol;
			YDim = territoryGrid.NumRows;
		}

		public bool ClearAllMatches (Player player)
		{
			bool needsFill = false;

			if (piecesToClear.Count > 0)
				piecesToClear.Clear ();
			
			int checkOffset = 3;
			int xCheck = 2;
			int yCheck = 2;

			for (int y = 0; y < YDim; y++)
			{
				for (int x = 0; x < XDim; x++)
				{
					if (x == xCheck || y == yCheck)
					{
						GridObject checkObject = gameGrid.GetGridObject (x, y);
						if (checkObject.IsMatchable && QuickCheckMatch (checkObject))
						{
							needsFill = true;
							if (!piecesToClear.Contains (checkObject.GetComponent<GridObject> ()))
							{
								List<GridObject> matching = GetAllMatched (checkObject);
								for (int j = 0; j < matching.Count; j++)
								{
									if (!piecesToClear.Contains (matching[j]))
									{
										piecesToClear.Add (matching [j]);
									}
								}
							}
						}
						if (x == xCheck)
							xCheck += checkOffset;
					}
				}
				xCheck = 2;

				if (y == yCheck)
					yCheck += checkOffset;
			}
			if (needsFill)
			{
				ContestTerritory (piecesToClear.ToArray (), player);
				ClearListPieces (piecesToClear);
			}
			return needsFill;
		}
		protected void ClearListPieces (List<GridObject> list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				if (list [i].GetComponent<IClearable>().Clear ())
				{
					gameGrid.SetToEmpty (list [i].X, list [i].Y);
				}
			}
		}

		public bool QuickCheckMatch (GridObject checkObject)
		{
			if (QuickcCheckHorizontal (checkObject, false) || QuickCheckVertical (checkObject, false))
				return true;
			else
				return false;
		}
		public bool QuickCheckMatch (GridObjectType type, int x, int y)
		{
			if (QuickcCheckHorizontal (type, x, y) || QuickCheckVertical (type, x, y))
				return true;
			else
				return false;
		}
		private bool QuickCheckVertical (GridObject checkObject, bool checkPlayer = true)
		{
			Player player = checkObject.Player;
			GridObjectType type = checkObject.GridObjectType;
			int x = checkObject.X;
			int checkY = checkObject.Y;

			int consecutiveCount = 0;

			bool includePlayerCheck = !checkPlayer;

			for (int dir = 0; dir < 2; dir++)
			{
				for (int yOffset = 1; yOffset < 3; yOffset++)
				{
					int y = dir == 0 ? checkY - yOffset : checkY + yOffset;
			
					if (!CheckBounds(x,y))
						break;

					GridObject otherObject = gameGrid.GetGridObject (x, y);
					if (otherObject.GridObjectType == type && (!checkPlayer || (checkPlayer && otherObject.Player == player)))
						consecutiveCount++;
					else
						break;
				}
				if (consecutiveCount >= 2)
					return true;
			}
			return false;
		}
		private bool QuickCheckVertical (GridObjectType type, int x, int checkY)
		{
			int consecutiveCount = 0;

			for (int dir = 0; dir < 2; dir++)
			{
				for (int yOffset = 1; yOffset < 3; yOffset++)
				{
					int y = dir == 0 ? checkY - yOffset : checkY + yOffset;

					if (!CheckBounds (x, y))
						break;

					GridObject otherObject = gameGrid.GetGridObject (x, y);
					if (otherObject.GridObjectType == type)
						consecutiveCount++;
					else
						break;
				}
				if (consecutiveCount >= 2)
					return true;
			}
			return false;
		}

		private bool QuickcCheckHorizontal (GridObject checkObject, bool checkPlayer = true)
		{
			Player player = checkObject.Player;
			GridObjectType type = checkObject.GridObjectType;
			int checkX = checkObject.X;
			int y = checkObject.Y;

			int consecutiveCount = 0;

			for (int dir = 0; dir < 2; dir++)
			{
				for (int xOffset = 1; xOffset < 3; xOffset++)
				{
					int x = dir == 0 ? checkX - xOffset : checkX + xOffset;

					if (!CheckBounds (x, y))
						break;

					GridObject otherObject = gameGrid.GetGridObject (x, y);
					if (otherObject.GridObjectType == type && (!checkPlayer || (checkPlayer && otherObject.Player == player)))
						consecutiveCount++;
					else
						break;
				}
				if (consecutiveCount >= 2)
					return true;
			}
			return false;
		}
		private bool QuickcCheckHorizontal (GridObjectType type, int checkX, int y)
		{
			int consecutiveCount = 0;

			for (int dir = 0; dir < 2; dir++)
			{
				for (int xOffset = 1; xOffset < 3; xOffset++)
				{
					int x = dir == 0 ? checkX - xOffset : checkX + xOffset;

					if (!CheckBounds (x, y))
						break;

					GridObject otherObject = gameGrid.GetGridObject (x, y);
					if (otherObject.GridObjectType == type)
						consecutiveCount++;
					else
						break;
				}
				if (consecutiveCount >= 2)
					return true;
			}
			return false;
		}

		private List<GridObject> GetAllMatched (GridObject matchObject)
		{
			matchingPieces.Clear ();
			GetMatchingRecursive (matchObject);
			return matchingPieces;
		}
		private void GetMatchingRecursive(GridObject matchObject)
		{
			Player player = matchObject.Player;
			GridObjectType type = matchObject.GridObjectType;
			int checkX = matchObject.X;
			int checkY = matchObject.Y;

			for (int dir = 0; dir < 2; dir++)
			{
				int x = dir == 0 ? checkX - 1 : checkX + 1;
				int y = dir == 0 ? checkY - 1 : checkY + 1;

				MatchCheck (x, checkY, player, type, false);
				MatchCheck (checkX, y, player, type, false);
			}
		}
		private void MatchCheck(int x, int y, Player player, GridObjectType gridObjectType, bool checkPlayer = true)
		{
			if (CheckBounds (x, y))
			{
				GridObject otherObject = gameGrid.GetGridObject (x, y);
				if (otherObject.GridObjectType == gridObjectType && (!checkPlayer || (checkPlayer && otherObject.Player == player)))
				{
					if (!matchingPieces.Contains (otherObject))
					{
						matchingPieces.Add (otherObject);
						GetMatchingRecursive (otherObject);
					}
				}
			}
		}

		private void ContestTerritory(GridObject[] cleared, Player player)
		{
			for (int i = 0; i < cleared.Length; i++)
			{
				//Player player = cleared[i].Player;
				int checkX = cleared [i].X;
				int checkY = cleared [i].Y;

				bool adjacentContest = false;

				if(cleared[i].Player != player)
				{
					ContestCheck (checkX, checkY, player, 3);
					gameGrid.SetToEmpty (checkX, checkY);
				}

				for (int dir = 0; dir < 2; dir++)
				{ 
					int x = dir == 0 ? checkX - 1 : checkX + 1;
					int y = dir == 0 ? checkY - 1 : checkY + 1;

					if (/*ContestCheck (x, checkY, player) ||*/ ContestCheck (checkX, y, player))
						adjacentContest = true;
				}

				if (adjacentContest)
					continue;

				for (int y = checkY; gameGrid.LoopCondition (Direction.Vertical, y); y += gameGrid.LoopIncriment (player, Direction.Up))
				{
					if (ContestCheck (checkX, y, player))
						break;
				}
			}
		}
		private bool ContestCheck(int x, int y, Player player, int dmgMtp = 1)
		{
			if (CheckBounds (x, y))
			{
				TerritoryGridObject territoryObject = territoryGrid.GetGridObject (x, y).GetComponent<TerritoryGridObject> ();
				if (territoryObject.Player != player || territoryObject.IsContestedTerritory)
				{
					territoryObject.ContestOwnership (player, dmgMtp);

					GridObject gridObjectOnTerritory = gameGrid.GetGridObject (x, y);
					if (gridObjectOnTerritory.IsClearable)
					{
						GridObject matchableGridObject = gridObjectOnTerritory.GetComponent<GridObject> ();
						if (!piecesToClear.Contains (matchableGridObject))
						{
							piecesToClear.Add (matchableGridObject);
						}
					}
					return true;
				}
			}
			return false;
		}

		public GridObject GetAdjacent (GridObject piece, Direction direction)
		{
			int x = piece.X;
			int y = piece.Y;
			switch (direction)
			{
				case Direction.Up:
					if (CheckBounds (x, y + 1))
						return gameGrid.GetGridObject (x, y + 1);
					break;

				case Direction.Down:
					if (CheckBounds (x, y - 1))
						return gameGrid.GetGridObject (x, y - 1);
					break;

				case Direction.Left:
					if (CheckBounds (x - 1, y))
						return gameGrid.GetGridObject (x - 1, y);
					break;

				case Direction.Right:
					if (CheckBounds (x + 1, y))
						return gameGrid.GetGridObject (x + 1, y);
					break;
			}

			return null;
		}
		private bool CheckBounds (int x, int y)
		{
			if (x < 0 || x >= XDim || y < 0 || y >= YDim)
				return false;
			else
				return true;
		}
	}
}