﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GeoWar
{
	public class GameGridFillController : MonoBehaviour {

		[Header ("Game Grid")]
		[SerializeField]
		protected GameGrid gameGrid;
		[SerializeField]
		protected TerritoryGrid territoryGrid;

		[Header ("Controllers")]
		[SerializeField]
		protected GameGridMatchController gameGridMatchController;

		public Action GridFillComplete;

		[Space]
		public float FillTime = 0.2f;

		private int XDim;
		private int YDim;

		private bool inverse = false;

		protected void Awake ()
		{
			XDim = territoryGrid.NumCol;
			YDim = territoryGrid.NumRows;
		}

		public IEnumerator Fill ()
		{
			bool addedToPlayerOne = true;
			bool addedToPlayerTwo = true;

			while (addedToPlayerOne || addedToPlayerTwo)
			{
				if(addedToPlayerOne)
					addedToPlayerOne = MovePieces (Player.Bottom);
				if(addedToPlayerTwo)
					addedToPlayerTwo = MovePieces (Player.Top);
				yield return new WaitForSeconds (FillTime);
			}

			inverse = !inverse;

			if (GridFillComplete != null)
				GridFillComplete ();
		}

		private bool MovePieces (Player player)
		{
			bool movedPiece = false;
			int playerDirectionOffsetY = player == Player.Bottom ? 1 : -1;

			int startingY = player == Player.Bottom ? YDim - 2 : 1;
			int loopIncrement = player == Player.Bottom ? -1 : 1;

			for (int y = startingY; gameGrid.LoopCondition(Direction.Vertical, y); y += gameGrid.LoopIncriment(player, Direction.Down))
			{
				for (int x = 0; x < XDim; x++)
				{
					if (territoryGrid.GetGridObject (x, y).Player != player)
						continue;

					GridObject gridPiece = gameGrid.GetGridObject(x,y);

					if (gridPiece.IsMovable)
					{
						if (territoryGrid.GetGridObject (x, y + playerDirectionOffsetY).Player != player)
							continue;

						GridObject pieceAhead = gameGrid.GetGridObject (x, y + playerDirectionOffsetY);

						if (pieceAhead.GridObjectType == GridObjectType.Empty)
						{
							IMovable matchableGridPiece = gridPiece.GetComponent<IMovable> ();
							matchableGridPiece.Move (gameGrid.GetGridObjectWorldPosition(x, y + playerDirectionOffsetY), FillTime, x, y + playerDirectionOffsetY);

							gameGrid.SetGridObject (gridPiece, x, y + playerDirectionOffsetY);
							gameGrid.SetToEmpty (x, y);

							movedPiece = true;
						} else 
						{
							for (int diag = -1; diag <= 1; diag++)
							{
								if (diag == 0)
									continue;

								int diagX = x + diag;
								if (inverse)
									diagX = x - diag;

								if (!CheckBounds (diagX, y + playerDirectionOffsetY))
									continue;
								if (territoryGrid.GetGridObject (diagX, y + playerDirectionOffsetY).Player != player)
									continue;

								GridObject diagonalPiece = gameGrid.GetGridObject(diagX, y + playerDirectionOffsetY);

								if (diagonalPiece.GridObjectType == GridObjectType.Empty)
								{
									bool hasPieceAbove = true;
									for (int aboveY = y; gameGrid.LoopCondition(Direction.Vertical, aboveY); aboveY += gameGrid.LoopIncriment(player, Direction.Down))
									{
										GridObject pieceAboveDiagonal = gameGrid.GetGridObject( diagX, aboveY );
																				
										if (territoryGrid.GetGridObject (diagX, aboveY).Player != player)
										{
											hasPieceAbove = false;
											break;
										}else if (pieceAboveDiagonal.GridObjectType == GridObjectType.Empty)
											continue;
										else if (pieceAboveDiagonal.IsMovable)
											break;

										hasPieceAbove = false;
									}
									if (!hasPieceAbove)
									{
										IMovable matchableGridPiece = gridPiece.GetComponent<IMovable> ();
										matchableGridPiece.Move (gameGrid.GetGridObjectWorldPosition (diagX, y + playerDirectionOffsetY), FillTime, diagX, y + playerDirectionOffsetY);

										gameGrid.SetGridObject (gridPiece, diagX, y + playerDirectionOffsetY);
										gameGrid.SetToEmpty (x, y);

										movedPiece = true;
										break;
									}
								}
							}
						}
					}
				}
			}

			bool addedNew = AddNewPiecesToGrid (player);
			if (addedNew)
				movedPiece = true;

			
			return movedPiece;
		}
		private bool AddNewPiecesToGrid (Player player)
		{
			int yCoord = player == Player.Bottom ? 0 : YDim - 1;
			int yInitCoord = player == Player.Bottom ? -1 : YDim;

			bool addedNew = false;
			for (int x = 0; x < XDim; x++)
			{
				if (gameGrid.GetGridObject (x, yCoord).GridObjectType == GridObjectType.Empty && territoryGrid.GetGridObject(x, yCoord).Player == player)
				{
					GridObjectType type = GridObjectType.Cross;

					do
					{
						type = (GridObjectType)UnityEngine.Random.Range (0, (int)GridObjectType.MatchableCount);
					}
					while (gameGridMatchController.QuickCheckMatch (type, x, yCoord));
					
					var newPiece = gameGrid.AddMatchableGridObject (type, player, x, yCoord);

					if (newPiece.IsMovable)
					{
						IMovable newPieceMovable = newPiece.GetComponent<IMovable> ();
						newPieceMovable.Set (gameGrid.GetGridObjectWorldPosition (x, yInitCoord));
						newPieceMovable.Move (gameGrid.GetGridObjectWorldPosition (x, yCoord), FillTime);
					} else
					{

					}
					addedNew = true;
				}
			}
			return addedNew;
		}
		private bool CheckBounds (int x, int y)
		{
			if (x < 0 || x >= XDim || y < 0 || y >= YDim)
				return false;
			else
				return true;
		}
	}
}