﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public class GameController : MonoBehaviour {

		[Header ("Controllers")]
		[SerializeField]
		protected GameGridController gameGridController;

		protected void Awake ()
		{
			gameGridController.TurnComplete += OnTurnComplete;
		}

		protected void Start ()
		{
			//Do into animations etc
			StartCoroutine (DoAfterDelay (0.5f, gameGridController.BeginGame));
		}

		protected IEnumerator DoAfterDelay(float delay, Action action)
		{
			yield return new WaitForSeconds (delay);
			if (action != null)
				action ();
		}

		private void OnTurnComplete (Player player)
		{
			//Do change Turn animation etc;
			Player nextPlayer = Player.None;

			if (player == Player.Bottom)
				nextPlayer = Player.Top;
			else if (player == Player.Top)
				nextPlayer = Player.Bottom;
			else
				nextPlayer = Player.Bottom;

			gameGridController.BeginTurn (nextPlayer);
		}
	}
}