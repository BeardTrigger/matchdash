﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public enum Direction
	{
		Up,
		Down,
		Left,
		Right,
		Horizontal,
		Vertical,
		None
	}

	public class GameGridInteractionController : MonoBehaviour
	{
		[Header ("Grids")]
		[SerializeField]
		protected GameGrid gameGrid;

		public Action<GridObject, Direction> ValidSwipeInteraction;
		public Action<GridObject> ValidClickInteraction;

		protected GridObject pressed;
		protected bool hasPressed = false;
		protected bool hasLeft = false;

		protected bool clicked = false;
		protected bool swiped = false;

		protected Vector3 pointerDownPosition;
		protected Direction swipeDirection;

		protected Player playerInteracting = Player.Bottom;

		protected Coroutine listenRoutine;

		public void Subscribe ()
		{
			gameGrid.SubcribeToInteractionReporting (OnPointerDown, OnPointerUp, OnPointerEnter, OnPointerExit);
		}

		public void StartListeningForPlayerValidInteraction(Player player)
		{
			playerInteracting = player;
			listenRoutine = StartCoroutine (ListenForReports ());
		}
		public void StopListening ()
		{
			StopCoroutine (listenRoutine);
		}

		protected IEnumerator ListenForReports ()
		{
			bool valid = false;
			while (!valid)
			{
				if (clicked)
				{
					if (ValidClickInteraction != null)
						ValidClickInteraction (pressed);
					pressed = null;
					clicked = false;
				}

				if (swiped)
				{
					if (ValidSwipeInteraction != null)
						ValidSwipeInteraction (pressed, GetSwpie ());

					hasLeft = false;
					pressed = null;
					swiped = false;
				}

				yield return null;
			}
		}

		private void OnPointerExit (GridObject obj)
		{
			//Debug.Log ("POINTER EXIT");
			if (pressed != null)
				hasLeft = true;
		}

		private void OnPointerEnter (GridObject obj)
		{
			//Debug.Log ("POINTER ENTER");
		}

		private void OnPointerUp (GridObject obj)
		{
			//Debug.Log ("POINTER UP");

			if (obj.Player != playerInteracting)
				return;

			if (!hasLeft)
			{
				clicked = true;
				return;
			}

			swiped = true;
		}

		private void OnPointerDown (GridObject obj)
		{
		//	Debug.Log ("POINTER DOWN");
			if (obj.Player == playerInteracting)
			{
				pressed = obj;
				pointerDownPosition = Input.mousePosition;
			}
		}

		protected Direction GetSwpie ()
		{
			var dir = (Input.mousePosition - pointerDownPosition).normalized;
			Direction swipeDirection = Direction.None;

			if (Mathf.Abs (dir.x) > Mathf.Abs (dir.y))
			{
				if (dir.x > 0f)
				{
					swipeDirection = Direction.Right;
				} else
				{
					swipeDirection = Direction.Left;
				}
			} else
			{
				if (dir.y > 0)
				{
					swipeDirection = Direction.Up;
				} else
				{
					swipeDirection = Direction.Down;
				}
			}

			return swipeDirection;
		}
	}
}