﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public class GameGridController : MonoBehaviour
	{

		[Header ("Grids")]
		[SerializeField]
		protected TerritoryGrid territoryGrid;
		[SerializeField]
		protected GameGrid gameGrid;

		[Header ("SubControllers")]
		[SerializeField]
		protected GameGridFillController gameGridFillController;
		[SerializeField]
		protected GameGridMatchController gameGridMatchController;
		[SerializeField]
		protected GameGridInteractionController	gameGridInteractionController;

		[Header ("Rules")]
		[SerializeField]
		protected bool HasToBeMatchToSwap = false;

		public Action<Player> TurnComplete;
		protected Player currentTurn = Player.None;


		protected void Start ()
		{
			gameGridInteractionController.Subscribe ();
			gameGridInteractionController.ValidClickInteraction += OnValidClick;
			gameGridInteractionController.ValidSwipeInteraction += OnValidSwipe;

			gameGridFillController.GridFillComplete += OnGridFillComplete;
		}

		public void BeginGame ()
		{
			StartCoroutine (gameGridFillController.Fill ());
		}

		public void BeginTurn(Player player)
		{
			currentTurn = player;
			gameGridInteractionController.StartListeningForPlayerValidInteraction (player);
		}

		protected void OnGridFillComplete ()
		{
			ClearAnyMatches ();
		}

		protected void ClearAnyMatches ()
		{
			if(gameGridMatchController.ClearAllMatches (currentTurn))
			{
				StartCoroutine (gameGridFillController.Fill ());
			} else
			{
				//Change Player Controll
				if(TurnComplete != null)
					TurnComplete (currentTurn);
			}
		}


		private void OnValidSwipe (GridObject obj, Direction dir)
		{
			if(Swap (obj, dir))
			{
				//Stop Listening to player
				gameGridInteractionController.StopListening ();
			}
			
		}

		private void OnValidClick (GridObject obj)
		{

		}

		
		public bool Swap (GridObject piece, Direction direction)
		{
			bool valid = false;
			GridObject otherPiece = gameGridMatchController.GetAdjacent (piece, direction);
			if (otherPiece == null || otherPiece.Player != piece.Player)
				return valid;
			else
			{
				valid = true;
				SwapPieces (piece, otherPiece);
			}

			return valid;
		}

		private void SwapPieces (GridObject piece1, GridObject piece2)
		{
			if (piece1.IsMovable && piece2.IsMovable)
			{
				int p1x = piece1.X;
				int p1y = piece1.Y;
				int p2x = piece2.X;
				int p2y = piece2.Y;

				// Swap refs in gridPieces so checking is correct
				gameGrid.SetGridObject (piece2, p1x, p1y);
				gameGrid.SetGridObject (piece1, p2x, p2y);

				bool doSwap = false;
				bool p1HasMatch = false;
				bool p2HasMatch = false;
				// Check for match before swapping
				p1HasMatch = gameGridMatchController.QuickCheckMatch(piece1);
				p2HasMatch = gameGridMatchController.QuickCheckMatch(piece2);

				if (HasToBeMatchToSwap)
				{
					if (p1HasMatch || p2HasMatch)
						doSwap = true;

					//if (!doSwap)
					//{
					//	piece1.MovablePiece.Move (p2x, p2y, FillTime / 2f, () => { piece1.MovablePiece.Move (p1x, p1y, FillTime / 2f, null); });
					//	piece2.MovablePiece.Move (p1x, p1y, FillTime / 2f, () => { piece2.MovablePiece.Move (p2x, p2y, FillTime / 2f, null); });
					//}
				} else
					doSwap = true;// Swap regardless

				if (doSwap)
				{
					// Swap'em
					piece1.GetComponent<IMovable> ().Move (gameGrid.GetGridObjectWorldPosition (p2x, p2y), gameGridFillController.FillTime, p2x, p2y, OnSwapComplete);
					piece2.GetComponent<IMovable> ().Move (gameGrid.GetGridObjectWorldPosition (p1x, p1y), gameGridFillController.FillTime, p1x, p1y, null);
				} else
				{
					// Return references
					gameGrid.SetGridObject (piece1, p1x, p1y);
					gameGrid.SetGridObject (piece2, p2x, p2y);
				}
			}
		}
		private void OnSwapComplete ()
		{
			ClearAnyMatches ();
		}
	}
}

