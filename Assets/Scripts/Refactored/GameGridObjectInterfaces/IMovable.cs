﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable  {

	void Set (Vector3 position);
	void Move (Vector3 position, float time, Action action = null);
	void Move (Vector3 position, float time, int x, int y, Action action = null);
}
