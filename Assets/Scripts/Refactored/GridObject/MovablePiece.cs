﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovablePiece : Mover {
	
	private IEnumerator moveCoroutine;

	private Action OnMoveFinished = null;

	public override void Move(int newX, int newY, float time, Action onMoveFinished) {
		if (moveCoroutine != null) {
			StopCoroutine(moveCoroutine);
		}

		OnMoveFinished = onMoveFinished;

		moveCoroutine = MoveCoroutine(newX, newY, time);
		StartCoroutine(moveCoroutine);
	}

	public override void Move (Vector3 position, float time, Action onMoveFinished)
	{
		if (moveCoroutine != null)
		{
			StopCoroutine (moveCoroutine);
		}

		OnMoveFinished = onMoveFinished;

		moveCoroutine = MoveCoroutine (position, time);
		StartCoroutine (moveCoroutine);
	}

	public IEnumerator MoveCoroutine(int newX, int newY, float time) {
		//piece.X = newX;
		//piece.Y = newY;

		Vector3 startPos = transform.position;
		Vector3 endPos = Grid.GetGridWorldPosition (newX, newY);

		for (float t = 0; t <= 1 * time; t += Time.deltaTime) {
			transform.position = Vector3.Lerp(startPos, endPos, t / time);
			yield return 0;
		}
		
		transform.position = Grid.GetGridWorldPosition(newX, newY);

		moveCoroutine = null;

		if (OnMoveFinished != null)
			OnMoveFinished();

		OnMoveFinished = null;
	}

	public IEnumerator MoveCoroutine (Vector3 position, float time)
	{
		Vector3 startPos = transform.position;
		Vector3 endPos = position;

		for (float t = 0; t <= 1 * time; t += Time.deltaTime)
		{
			transform.position = Vector3.Lerp (startPos, endPos, t / time);
			yield return 0;
		}

		transform.position = position;

		moveCoroutine = null;

		if (OnMoveFinished != null)
			OnMoveFinished ();

		OnMoveFinished = null;
	}
}
