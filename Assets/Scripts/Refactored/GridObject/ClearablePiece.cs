﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public class ClearablePiece : Clearer
	{
		public int ClearCountThreashold = 1;

		public override bool Clear ()
		{
			ClearCount++;

			if (ClearCount < ClearCountThreashold)
				return false;

			if (PieceCleared != null)
				PieceCleared ();

			return true;
		}
	}
}
