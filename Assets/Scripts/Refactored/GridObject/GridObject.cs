﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public enum GridObjectType
	{
		Cross,
		Triangle,
		Hexagon,
		Kite,
		Donut,
		Rhombus,
		Trapazoid,
		MatchableCount,
		Territory,
		Empty,
	}

	public class GridObject : MonoBehaviour
	{
		public GridObjectType GridObjectType { get; protected set; }
		public Player Player = Player.None;// { get; protected set; }

		/// <summary>
		/// From, To
		/// </summary>
		public Action<Player,Player> PlayerChanged;

		public int X { get; protected set; }
		public int Y { get; protected set; }

		public virtual bool IsMovable { get { return false; } }
		public virtual bool IsClearable { get { return false; } } 
		public virtual bool IsMatchable { get { return false; } }
		public virtual bool IsInteractable { get { return false; } }

		protected SpriteRenderer [] spriteRenderers;
		protected Collider2D [] colliders;

		public InteractionReporter InteractionReporter;

		[SerializeField]
		protected GameGridObjectDatabase gameGridObjectDatabaseSO;

		protected bool isEnabled = false;
		public bool Enabled { get { return isEnabled; } }

		private bool hasSpriteRenderers = false;
		private bool hasColliders = false;

		protected virtual void Awake ()
		{
			spriteRenderers = GetComponentsInChildren<SpriteRenderer> (true);
			if (spriteRenderers != null)
				hasSpriteRenderers = true;

			colliders = GetComponentsInChildren<Collider2D> (true);
			if (colliders != null)
				hasColliders = true;
		}

		public virtual void Initialize(GridObjectType gridObjectType, Player player, int x, int y)
		{
			GridObjectType = gridObjectType;
			SetOwnership(player);
			X = x;
			Y = y;
		}

		public virtual void SetOwnership (Player player) {

			if (PlayerChanged != null)
				PlayerChanged (Player, player);

			Player = player;
		}

		public virtual void EnableGridObject ()
		{
			if (hasSpriteRenderers)
			{
				for (int i = 0; i < spriteRenderers.Length; i++)
				{
					spriteRenderers [i].enabled = true;
				}
			}

			if (hasColliders)
			{
				for (int i = 0; i < colliders.Length; i++)
				{
					colliders [i].enabled = true;
				}
			}

			isEnabled = true;
		}

		public virtual void DisableGridObject ()
		{
			if (hasSpriteRenderers)
			{
				for (int i = 0; i < spriteRenderers.Length; i++)
				{
					spriteRenderers [i].enabled = false;
				}
			}

			if (hasColliders)
			{
				for (int i = 0; i < colliders.Length; i++)
				{
					colliders [i].enabled = false;
				}
			}

			isEnabled = false;
		}
	}
}
