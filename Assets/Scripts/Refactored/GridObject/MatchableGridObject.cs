﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public class MatchableGridObject : GridObject, IClearable, IMovable
	{
		[Header ("Display Object References")]
		[SerializeField]
		protected Transform displayParentTransform;
		[SerializeField]
		protected SpriteRenderer centerSpriteRenderer;
		[SerializeField]
		protected SpriteRenderer blurSpriteRenderer;

		public override bool IsMovable { get { return true; } }
		public override bool IsClearable { get { return true; } }
		public override bool IsMatchable { get { return true; } }
		public override bool IsInteractable { get { return true; } }

		public Action<GridObject> MatchableCleared;

		protected Mover mover;
		protected Clearer clearer;

		protected override void Awake ()
		{
			base.Awake ();

			mover = GetComponent<MovablePiece> ();
			clearer = GetComponent<ClearablePiece> ();
			clearer.PieceCleared += OnCleared;
		}

		public override void Initialize (GridObjectType gridObjectType, Player player, int x, int y)
		{
			base.Initialize (gridObjectType, player, x, y);

			var DisplayProperties = gameGridObjectDatabaseSO.GetDisplayProperties (gridObjectType);
			
			centerSpriteRenderer.sprite = DisplayProperties.CenterSprite;
			blurSpriteRenderer.sprite = DisplayProperties.BlurSprite;
			
			for (int i = 0; i < displayParentTransform.childCount; i++)
			{
				displayParentTransform.GetChild (i).localPosition = DisplayProperties.LocalPosition;
				displayParentTransform.GetChild (i).localScale = DisplayProperties.LocalScale;
			}

			clearer.ResetClearer();
		}
		public override void SetOwnership (Player player)
		{
			base.SetOwnership (player);
		
			blurSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (player).ActiveMaterial;
		}

		public bool Clear ()
		{
			return clearer.Clear ();
		}
		protected void OnCleared ()
		{
			DisableGridObject ();

			if (MatchableCleared != null)
				MatchableCleared (this);
		}

		public void Set (Vector3 position)
		{
			transform.position = position;
		}
		public void Move (Vector3 position, float time, Action action = null)
		{
			mover.Move (position, time, action);
		}
		public void Move (Vector3 position, float time, int x, int y, Action action = null)
		{
			X = x;
			Y = y;
			Move (position, time, action);
		}
	}
}