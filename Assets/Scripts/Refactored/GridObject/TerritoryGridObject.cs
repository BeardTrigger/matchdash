﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	
	public class TerritoryGridObject : GridObject
	{
		[Header ("Display Object References")]
		[SerializeField]
		protected Transform capturedParentTransform;
		[SerializeField]
		protected SpriteRenderer capturedSpriteRenderer;
		[Space]
		[SerializeField]
		protected Transform contestedParentTransform;
		[SerializeField]
		protected SpriteRenderer contestedTopSpriteRenderer;
		[SerializeField]
		protected SpriteRenderer contestedMiddleSpriteRenderer;
		[SerializeField]
		protected SpriteRenderer contestedBottomSpriteRenderer;

		protected SpriteRenderer[] contestedSpriteRenderersList;

		
		protected int contestCountBottom = 0;
		protected int contestCountTop = 0;

		private int contestCountToOwn = 3;

		protected Player hasMajority { get
			{
				if ((contestCountBottom / (float)contestCountToOwn) > 0.5f)
					return Player.Bottom;
				if ((contestCountTop / (float)contestCountToOwn) > 0.5f)
					return Player.Top;
				else
					return Player.None;
			}
		}

		public bool IsContestedTerritory { get { return (contestCountBottom > 0 && contestCountBottom < contestCountToOwn) || (contestCountTop > 0 && contestCountTop < contestCountToOwn); } }

		protected override void Awake ()
		{
			base.Awake ();
			contestedSpriteRenderersList = contestedParentTransform.GetComponentsInChildren<SpriteRenderer> (true);
		}

		public override void Initialize (GridObjectType gridObjectType, Player player, int x, int y)
		{
			base.Initialize (gridObjectType, player, x, y);
			SetDisplay (true);
		}

		public override void SetOwnership(Player player)
		{
			base.SetOwnership (player);
			SetDisplay ();
		}

		protected void SetDisplay (bool contestDisplay = false)
		{
			capturedSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player).InactiveMaterial;
			if (contestDisplay)
			{
				contestedTopSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player).ActiveMaterial;
				contestedMiddleSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player).ActiveMaterial;
				contestedBottomSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player).ActiveMaterial;
			}
		}

		public void ContestOwnership(Player player, int addValue = 1)
		{
			if (!IsContestedTerritory)
				EnableContest ();

			bool fullOwnership = false;

			if(player == Player.Bottom)
			{
				contestCountBottom += addValue;
				contestCountBottom = Mathf.Min (contestCountBottom, contestCountToOwn);

				contestCountTop = Mathf.Min (contestCountTop, (contestCountToOwn - contestCountBottom));

				if (contestCountBottom == contestCountToOwn)
					fullOwnership = true;

			} else if(player == Player.Top)
			{
				contestCountTop += addValue;
				contestCountTop = Mathf.Min (contestCountTop, contestCountToOwn);

				contestCountBottom = Mathf.Min (contestCountBottom, (contestCountToOwn - contestCountTop));

				if (contestCountTop == contestCountToOwn)
					fullOwnership = true;
			}

			if (Player != hasMajority)
			{
				SetOwnership (player);
			}
			if (fullOwnership)
			{
				DisableContest ();
			} else
				DisplayContest ();
		}

		protected void DisplayContest ()
		{
			if (contestCountBottom >= 1)
			{
				contestedBottomSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player.Bottom).ActiveMaterial;
			}
			if(contestCountBottom >= 2)
			{
				contestedMiddleSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player.Bottom).ActiveMaterial;
			}

			if (contestCountTop >= 1)
			{
				contestedTopSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player.Top).ActiveMaterial;
			}
			if (contestCountTop >= 2)
			{
				contestedMiddleSpriteRenderer.material = gameGridObjectDatabaseSO.GetGameTeamsMaterial (Player.Top).ActiveMaterial;
			}
		}

		protected void DisableContest ()
		{
			contestedParentTransform.gameObject.SetActive (false);
		}
		protected void EnableContest ()
		{
			contestedParentTransform.gameObject.SetActive (true);
		}

		public override void EnableGridObject ()
		{
			base.EnableGridObject ();
			if (IsContestedTerritory)
			{
				EnableContest ();
				capturedSpriteRenderer.enabled = false;
			} else
			{
				DisableContest ();
				capturedSpriteRenderer.enabled = true;
			}
		}
	}
}