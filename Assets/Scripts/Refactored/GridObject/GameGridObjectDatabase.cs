﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	[CreateAssetMenu]
	public class GameGridObjectDatabase : ScriptableObject { 

		[System.Serializable]
		public struct GridObjectDisplay
		{
			public GridObjectType GridObjectType;
			public Sprite CenterSprite;
			public Sprite BlurSprite;
			public Vector3 LocalPosition;
			public Vector3 LocalScale;
		}
		public GridObjectDisplay [] GridObjectDisplays;
		private Dictionary<GridObjectType, GridObjectDisplay> GridObjectDisplayDict;
		public GridObjectDisplay GetDisplayProperties (GridObjectType GridObjectType)
		{
			if(GridObjectDisplayDict == null)
			{
				GridObjectDisplayDict = new Dictionary<GridObjectType, GridObjectDisplay> ();
				for (int i = 0; i < GridObjectDisplays.Length; i++)
				{
					if (!GridObjectDisplayDict.ContainsKey (GridObjectDisplays [i].GridObjectType))
					{
						GridObjectDisplayDict.Add (GridObjectDisplays [i].GridObjectType, GridObjectDisplays [i]);
					}
				}
			}

			if (!GridObjectDisplayDict.ContainsKey (GridObjectType))
			{
				Debug.LogError ("GridObjectDisplayDict Does Not Contail the GridObject You are Looking for");
				return new GridObjectDisplay ();
			} else
			{
				return GridObjectDisplayDict [GridObjectType];
			}
		}


		[System.Serializable]
		public struct GameTeamsMaterial
		{
			public Player Player;
			public Material ActiveMaterial;
			public Material InactiveMaterial;
		}
		public GameTeamsMaterial [] GameTeamsMaterials;
		private Dictionary<Player, GameTeamsMaterial> GameTeamsMaterialDict;
		public GameTeamsMaterial GetGameTeamsMaterial (Player player)
		{
			if (GameTeamsMaterialDict == null)
			{
				GameTeamsMaterialDict = new Dictionary<Player, GameTeamsMaterial> ();
				for (int i = 0; i < GameTeamsMaterials.Length; i++)
				{
					if (!GameTeamsMaterialDict.ContainsKey (GameTeamsMaterials [i].Player))
					{
						GameTeamsMaterialDict.Add (GameTeamsMaterials [i].Player, GameTeamsMaterials [i]);
					}
				}
			}
			
			var playerOwner = player;

			if (!GameTeamsMaterialDict.ContainsKey (playerOwner))
			{
				Debug.LogError ("GridObjectDisplayDict Does Not Contail the GridObject You are Looking for");
				return new GameTeamsMaterial ();
			} else
			{
				return GameTeamsMaterialDict [playerOwner];
			}
		}
	}
}
