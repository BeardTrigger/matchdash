﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public class InteractionReporter : MonoBehaviour
	{
		public Action<GridObject> PointerDown;
		public Action<GridObject> PointerUp;
		public Action<GridObject> PointerExit;
		public Action<GridObject> PointerEnter;

		public GridObject GridObject;

		protected void OnMouseDown ()
		{
			if (PointerDown != null)
				PointerDown (GridObject);
		} 

		protected void OnMouseExit ()
		{
			if (PointerExit != null)
				PointerExit(GridObject);
		}

		protected void OnMouseUp ()
		{
			if (PointerUp != null)
				PointerUp (GridObject);
		}

		protected void OnMouseEnter ()
		{
			if (PointerEnter != null)
				PointerEnter (GridObject);
		}
	}
}