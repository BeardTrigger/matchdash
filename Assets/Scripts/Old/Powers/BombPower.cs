﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombPower : Power {

	[Header("Behavior and Settings")]
	public float[] RadiusLvls;

	private CircleCollider2D circleCollider;
	
	protected override void Awake() {
		base.Awake();
		circleCollider = Collider.GetComponent<CircleCollider2D>();
	}

	
	protected override void DoPower() {
		// Set Radious of explosion
		circleCollider.radius = RadiusLvls[Lvl];

		Collider.SetActive(true);
		Collider.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);


		ContactFilter2D contactFilter2D = new ContactFilter2D();
		Collider2D[] overlaps = new Collider2D[Grid.XDim * Grid.YDim];
		var overlapping = circleCollider.OverlapCollider(contactFilter2D, overlaps);

		if(overlapping > 0) {
			List<GridPiece> PiecesToDestroy = new List<GridPiece>();

			for (int i = 0; i < overlaps.Length; i++) {
				if(overlaps[i] != null) {
					var piece = overlaps[i].gameObject.GetComponent<GridPiece>();
					if (piece != null)
						PiecesToDestroy.Add(piece);
				}
			}

			//Grid.ClearListPieces(PiecesToDestroy);
			Grid.BeginFill();
		}

		Collider.SetActive(false);
	}
}
