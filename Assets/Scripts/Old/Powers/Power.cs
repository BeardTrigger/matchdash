﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour {

	public Grid Grid;
	public GameObject Collider;

	public int activeThreashold = 10;
	public int countMax = 30;

	protected int Lvl { get { return ActivateCount / activeThreashold; } }

	public PieceType Type;
	public Color ActiveColor;
	public Color InactiveColor;

	public Sprite[] Count;
	public SpriteRenderer CountRenderer;

	private int activateCount;
	protected int ActivateCount {
		get { return activateCount; }
		set {
			activateCount = value;
			CountRenderer.sprite = Count[Lvl];
			if (Lvl > 0) {
				spriteRenderer.color = ActiveColor;
				ReadyToFire = true;
			}
			else
				spriteRenderer.color = InactiveColor;
		}
	}
	private int ActivateCountCollectedFromPower;

	private SpriteRenderer spriteRenderer;

	[Header("//Debug")]
	public bool ReadyToFire = true;

	protected virtual void Awake() {
		spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
	}

	bool fired = false;
	private IEnumerator FireOnNextPress() {
		fired = false;
		while (!fired) {
			if (Input.GetMouseButtonDown(0)) {
				fired = true;

				// Do Power
				DoPower();

				ActivateCount -= activeThreashold * Lvl; // Remove Cost Of Skill
				//ActivateCount += ActivateCountCollectedFromPower;
				//ActivateCountCollectedFromPower = 0;
			}
			else
				yield return null;
		}
		fired = false;
	}

	protected virtual void DoPower() { }

	private void OnMouseUpAsButton() {
		if (ReadyToFire) {

			StartCoroutine(FireOnNextPress());
			ReadyToFire = false;
		}
	}

	public virtual void PieceDestroyed() {
		if (fired) {
			ActivateCountCollectedFromPower++;
		}
		else {
			ActivateCount = Mathf.Min(countMax, ++ActivateCount);
		}
	}
}