﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningPower : Power {

	[Header("Behavior and Settings")]
	private CircleCollider2D circleCollider;

	public int[] StagesPerLvl;
	public int[] RepeatingPerLvl;

	protected override void Awake() {
		base.Awake();
		circleCollider = Collider.GetComponent<CircleCollider2D>();
	}

	protected override void DoPower() {
		// Set Radious of explosion
		Collider.SetActive(true);
		Collider.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);


		ContactFilter2D contactFilter2D = new ContactFilter2D();
		Collider2D[] overlaps = new Collider2D[Grid.XDim * Grid.YDim];
		var overlapping = circleCollider.OverlapCollider(contactFilter2D, overlaps);

		if (overlapping > 0) {
			List<GridPiece> PiecesToDestroy = new List<GridPiece>();

			for (int i = 0; i < overlaps.Length; i++) {
				if (overlaps[i] != null) {
					var piece = overlaps[i].gameObject.GetComponent<GridPiece>();
					if (piece != null)
						PiecesToDestroy.Add(piece);
				}
			}

			List<GridPiece> lightningBolts;

			var originPiece = PiecesToDestroy[0];
			int startX = originPiece.X;
			int startY = originPiece.Y;

			//lightningBolts = new List<GridPiece>();
			//lightningBolts.Add(originPiece);
			//lightningBolts.Add(Grid.GetPiece(startX - 1, startY));
			//lightningBolts.Add(Grid.GetPiece(startX + 1, startY));
			//lightningBolts.Add(Grid.GetPiece(startX, startY - 1));
			//lightningBolts.Add(Grid.GetPiece(startX, startY + 1));
			//for (int i = 0; i < RepeatingPerLvl[Lvl]; i++) {
			//	var up = GetNextLightning(Directions.UP, Grid.GetPiece(startX, startY + 1), StagesPerLvl[Lvl]);
			//	for (int j = 0; j < up.Count; j++) {
			//		if (!lightningBolts.Contains(up[j]))
			//			lightningBolts.Add(up[j]);
			//	}
			//
			//	var down = GetNextLightning(Directions.DOWN, Grid.GetPiece(startX, startY - 1), StagesPerLvl[Lvl]); for (int j = 0; j < down.Count; j++) {
			//		if (!lightningBolts.Contains(down[j]))
			//			lightningBolts.Add(down[j]);
			//	}
			//
			//	var left = GetNextLightning(Directions.LEFT, Grid.GetPiece(startX - 1, startY), StagesPerLvl[Lvl]); for (int j = 0; j < left.Count; j++) {
			//		if (!lightningBolts.Contains(left[j]))
			//			lightningBolts.Add(left[j]);
			//	}
			//
			//	var right = GetNextLightning(Directions.RIGHT, Grid.GetPiece(startX + 1, startY), StagesPerLvl[Lvl]); for (int j = 0; j < right.Count; j++) {
			//		if (!lightningBolts.Contains(right[j]))
			//			lightningBolts.Add(right[j]);
			//	}
			//
			//}

			//Grid.ClearListPieces(lightningBolts, 0.05f);
		}

		Collider.SetActive(false);
	}

	private List<GridPiece> GetNextLightning(Directions dir, GridPiece piece, int stages) {
		List<GridPiece> lightning = new List<GridPiece>();
		List<GridPiece> nextbolt = new List<GridPiece>();
		int stage = stages;
		if (stages >= 0 && piece != null) {
			if (!lightning.Contains(piece)) {
				lightning.Add(piece);
			}

			int randomFromDir = Random.Range(-1,2);
			GridPiece nextPiece = null;

			//switch (dir) {
			//	case Directions.UP:
			//		nextPiece = Grid.GetPiece(piece.X + randomFromDir, piece.Y + 1);
			//		break;
			//
			//	case Directions.DOWN:
			//		nextPiece = Grid.GetPiece(piece.X + randomFromDir, piece.Y - 1);
			//		break;
			//
			//	case Directions.LEFT:
			//		nextPiece = Grid.GetPiece(piece.X - 1, piece.Y + randomFromDir);
			//		break;
			//
			//	case Directions.RIGHT:
			//		nextPiece = Grid.GetPiece(piece.X + 1, piece.Y + randomFromDir);
			//		break;
			//
			//}

			if (nextPiece == null) {
				stage = -1;
			}
			else
				stage -= 1;

			nextbolt = GetNextLightning(dir, nextPiece, stage);

			for (int i = 0; i < nextbolt.Count; i++) {
				if (!lightning.Contains(nextbolt[i])) {
					lightning.Add(nextbolt[i]);
				}
			}
		}
		return lightning;
	}
}
