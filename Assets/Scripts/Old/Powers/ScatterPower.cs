﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScatterPower : Power {

	[Header("Behavior and Settings")]
	public float[] RadiusLvls;

	private CircleCollider2D circleCollider;

	protected override void Awake() {
		base.Awake();
		circleCollider = Collider.GetComponent<CircleCollider2D>();
	}


	protected override void DoPower() {
		// Set Radious of explosion
		circleCollider.radius = RadiusLvls[Lvl];

		Collider.SetActive(true);
		Collider.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);


		ContactFilter2D contactFilter2D = new ContactFilter2D();
		Collider2D[] overlaps = new Collider2D[Grid.XDim * Grid.YDim];
		var overlapping = circleCollider.OverlapCollider(contactFilter2D, overlaps);

		if (overlapping > 0) {
			List<GridPiece> PiecesToDestroy = new List<GridPiece>();

			for (int i = 0; i < overlaps.Length; i++) {
				if (overlaps[i] != null) {
					var piece = overlaps[i].gameObject.GetComponent<GridPiece>();
					if (piece != null)
						PiecesToDestroy.Add(piece);
				}
			}

			// Filter List Randomly

			// Reduction ration to remove
			float count = 0;
			if (Lvl == 1 || Lvl == 2)
				count = PiecesToDestroy.Count * 0.5f;
			else if (Lvl == 3)
				count = PiecesToDestroy.Count * 0.333f;
			else if (Lvl == 4)
				count = PiecesToDestroy.Count * 0.2f;

			for (int i = 0; i < Mathf.FloorToInt(count); i++) {
				int index = Random.Range(0, PiecesToDestroy.Count);
				PiecesToDestroy.RemoveAt(index);
			}

			//Grid.ClearListPieces(PiecesToDestroy);
			Grid.BeginFill();
		}

		Collider.SetActive(false);
	}
	
}
