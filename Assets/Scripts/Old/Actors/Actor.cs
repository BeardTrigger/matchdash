﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour {
	// Grid and its interactions
	protected Grid grid;

	public TurnType Type;

	public Action OnTurnComplete;
	public Action<TurnType> OnEndTurn;
	public Action<Actor> OnActorDestroyed;

	protected bool isActorsTurn;

	public virtual void TakeTurn() {
		isActorsTurn = true;
		grid.OnFillCompete += TurnComplete;
	}

	public virtual void Initialize(Grid grid, Action turnComplete, Action<TurnType> endTurn, Action<Actor> actorDestroyed) {
		this.grid = grid;

		OnTurnComplete = turnComplete;
		OnEndTurn = endTurn;
		OnActorDestroyed = actorDestroyed;
	}

	protected virtual void OnDisable() {
		if(OnActorDestroyed != null) {
			OnActorDestroyed(this);
		}
	}

	protected virtual void TurnComplete() {
		grid.OnFillCompete -= TurnComplete;

		if (Type == TurnType.PLAYER || Type == TurnType.OPPONENT) {
			if (OnEndTurn != null)
				OnEndTurn(Type);
			return;
		}

		if (OnTurnComplete != null)
			OnTurnComplete();
	}
}
