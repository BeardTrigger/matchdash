﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Actor {

	private GridPiece pressedPiece;

	private bool HasMadeASwapThisTurn = false;

	public override void Initialize(Grid grid, Action turnComplete, Action<TurnType> endTurn, Action<Actor> actorDestroyed) {
		base.Initialize(grid, turnComplete, endTurn, actorDestroyed);

		//grid.OnSwapMatched += WhenFillCompleteAfterSwap;
	}

	public override void TakeTurn() {
		base.TakeTurn();
		// TODO: Tell the user its their turn
		HasMadeASwapThisTurn = false;
		//Debug.Log("Player Takes Thier Turn");
	}

	public void PressPiece(GridPiece piece) {
		if (isActorsTurn && grid.IsIntractable && !HasMadeASwapThisTurn)
			pressedPiece = piece;
	}
	public void ReleasePiece(Directions dir) {
		if (pressedPiece == null)
			return;

		grid.Swap(pressedPiece, dir);
		HasMadeASwapThisTurn = true;

		pressedPiece = null;
	}

	private void OnMouseUpAsButton() {
		if (isActorsTurn) {
			isActorsTurn = false;
			TurnComplete();
		}
	}

	private void WhenFillCompleteAfterSwap() {
		if (isActorsTurn /*TODO: && Player does not have any powers of allies to adjust*/) {
			isActorsTurn = false;
			TurnComplete();
		}
	}
}
