﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyFourPiece : Actor {

	public PieceType AllyType;

	public PolygonCollider2D Collider;
	private GridPiece gridPiece;
	private void Awake() {
		gridPiece = GetComponent<GridPiece>();
	}

	public override void TakeTurn() {
		base.TakeTurn();

		ContactFilter2D contactFilter2D = new ContactFilter2D();
		Collider2D[] overlaps = new Collider2D[grid.XDim * grid.YDim];
		var overlapping = Collider.OverlapCollider(contactFilter2D, overlaps);

		if (overlapping > 0) {
			List<GridPiece> PiecesToDestroy = new List<GridPiece>();

			for (int i = 0; i < overlaps.Length; i++) {
				if (overlaps[i] != null) {
					var piece = overlaps[i].gameObject.GetComponent<GridPiece>();
					if (piece != null)
						if (piece != gridPiece)
							PiecesToDestroy.Add(piece);
				}
				else
					break;
			}

			//grid.ClearListPieces(PiecesToDestroy);
		}

		grid.ClearPiece(gridPiece);// Decay
		grid.BeginFill();
	}

	private void OnMouseUpAsButton() {
		//TODO: Check thats its player turn and that the player isn't Using a power
		AimTurret(null);
	}

	public void AimTurret(Action OnSet) {
		StartCoroutine(DoAimTurret(OnSet));
	}

	List<GridTileView> Highlighting = new List<GridTileView>();
	private IEnumerator DoAimTurret(Action OnSet) {
		bool set = false;
		while (!set) {
			//Vector3 myPos = Grid.GetGridWorldPosition(gridPiece.X, gridPiece.Y);
			Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			// Get Angle in Radians
			float AngleRad = Mathf.Atan2( mousePos.y - Collider.transform.position.y, mousePos.x - Collider.transform.position.x);
			// Get Angle in Degrees
			float AngleDeg = (180 / Mathf.PI) * AngleRad;
			// Rotate Object
			Collider.transform.rotation = Quaternion.Euler(0, 0, AngleDeg/* - 90f*/);


			if (Highlighting.Count > 0)
				grid.gridDisplay.BackToNeutralHighlight(Highlighting);

			Highlighting.Clear();

			ContactFilter2D contactFilter2D = new ContactFilter2D();
			contactFilter2D.layerMask.value = 13;
			Collider2D[] overlaps = new Collider2D[grid.XDim * grid.YDim];
			var overlapping = Collider.OverlapCollider(contactFilter2D, overlaps);

			for (int i = 0; i < overlaps.Length; i++) {
				if (overlaps[i] != null) {
					var piece = overlaps[i].gameObject.GetComponent<GridTileView>();
					if (piece != null)
						Highlighting.Add(piece);
				}
				else
					break;
			}

			grid.gridDisplay.Highlight(Highlighting, AllyType);

			yield return null;

			if (Input.GetMouseButtonUp(0)) {
				set = true;
				grid.gridDisplay.SetTileColor(Highlighting, AllyType);
			}
		}

		if (OnSet != null)
			OnSet();
	}

	protected override void OnDisable() {
		grid.gridDisplay.BackToNeutralHighlight(Highlighting, true);
		base.OnDisable();
	}
}
