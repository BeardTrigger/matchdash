﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placer : MonoBehaviour {


	public GameObject[] Prefabs;
	private List<GameObject> prefabList;

	public Material [] Materials;
	public bool SwapMaterial;

	public bool Flip;

	[Range (1, 20)]
	public int XCount;
	private int currentXCount;

	[Range (1, 30)]
	public int YCount;
	private int currentYCount;

	[Space]
	[Range(0f,1f)]
	public float Space = 0f;
	private float currentSpace;
	[Space]
	[Range (0f, 1f)]
	public float Buffer = 0;
	private float currentBuffer;

	private float screenWidthInWorldSpace;
	private float screenHeightInWorldSpace;

	private float xStartingPos;
	private float yStartingPos;

	public bool Tint = false;
	public Color odd = Color.white;
	public Color even = Color.grey;


	private void Awake ()
	{
		currentXCount = XCount;
		currentYCount = YCount;

		currentSpace = Space;
		currentBuffer = Buffer;

		prefabList = new List<GameObject> ();

		var MinPoint = Camera.main.ViewportToWorldPoint (new Vector3(0f, 0f, 0f));
		var MaxPoint = Camera.main.ViewportToWorldPoint (new Vector3 (1f, 1f, 1f));

		screenWidthInWorldSpace = MaxPoint.x - MinPoint.x;
		screenHeightInWorldSpace = MaxPoint.y - MinPoint.y;

		xStartingPos = MinPoint.x;
		yStartingPos = MinPoint.y;
	}

	// Use this for initialization
	void Start () {
		for (int i = 0; i < Prefabs.Length; i++)
		{
			for (int y = 0; y < 30; y++)
			{
				for (int x = 0; x < 20; x++)
				{
					var prefabGO = Instantiate (Prefabs[i], Vector3.zero, Quaternion.identity);
					prefabGO.SetActive (false);
					prefabList.Add (prefabGO);
				}
			}
		}
		Fit ();
	}
	
	// Update is called once per frame
	void Update () {
		if(currentXCount != XCount || currentSpace != Space || currentYCount != YCount || currentBuffer != Buffer)
		{
			currentXCount = XCount;
			currentYCount = YCount;

			currentSpace = Space;
			currentBuffer = Buffer;

			Fit ();
		}
	}

	protected void Fit ()
	{
		DisableAll ();
		float xStep = screenWidthInWorldSpace / (float)currentXCount;

		float yStep = (screenHeightInWorldSpace * (1 - currentBuffer)) / (float)currentYCount;
		for (int y = 0; y < currentYCount; y++)
		{
			for (int x = 0; x < currentXCount; x++)
			{
				int index = x + (y * 20);
				if (Prefabs.Length > 1)
					index = (index) + ((20 * 30) * Random.Range (0, Prefabs.Length));

				Vector3 pos = Vector3.zero;
				pos.x = ((xStep * (x + 1)) / 2f) + (xStartingPos + ((x * xStep) / 2f));
				pos.y = ((yStep * (y + 1)) / 2f) + (yStartingPos + ((currentBuffer * screenHeightInWorldSpace) / 2f) + ((y * yStep) / 2f));
				prefabList [index].transform.position = pos;

				if(Flip && y > currentYCount * 0.5f)
				{
					prefabList [index].transform.Rotate (Vector3.forward, 180f);
				}

				if (xStep < 1)
				{
					//Adjust Scale of Tile;
					Vector3 scale = Vector3.one;
					scale.x = xStep - (currentSpace / 2f);
					scale.y = xStep - (currentSpace / 2f);
					prefabList [index].transform.localScale = scale;
				}

				if (Tint)
				{
					if (y % 2 == 0)
					{
						prefabList [index].GetComponentInChildren<SpriteRenderer> ().color = even;
						if (x % 2 == 1)
							prefabList [index].GetComponentInChildren<SpriteRenderer> ().color = odd;
					} else
					{
						prefabList [index].GetComponentInChildren<SpriteRenderer> ().color = odd;
						if (x % 2 == 1)
							prefabList [index].GetComponentInChildren<SpriteRenderer> ().color = even;
					}
				}
				if(SwapMaterial && y < currentYCount * 0.5f)
				{
					prefabList [index].transform.GetChild (1).GetComponent<SpriteRenderer> ().material = Materials [0];
				}
				prefabList [index].SetActive (true);
			}
		}
		
	}

	private void DisableAll ()
	{
		for (int i = 0; i < prefabList.Count; i++)
		{
			prefabList [i].SetActive (false);
		}
	}
}
