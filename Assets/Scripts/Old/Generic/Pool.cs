﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Does Not Work for pools of GameObjects, Works for all other Reference Types
/// </summary>
/// <typeparam name="T"></typeparam>
public class Pool<T> : MonoBehaviour {

	private List<GameObject> objectPool;
	private List<T> referencePool;
	private int nextPooledObject = 0;
	public int PoolSize { get { return objectPool.Count; } }

	private GameObject GO;

	public Pool(GameObject poolObject, int poolSize) {

		GO = new GameObject ("PoolParent");
		GO.gameObject.hideFlags = HideFlags.HideAndDontSave;
		
		objectPool = new List<GameObject>();
		for (int i = 0; i < poolSize; i++) {
			objectPool.Add(GameObject.Instantiate(poolObject, GO.transform));
		}

		if (typeof(T) != typeof(GameObject)) {
			referencePool = new List<T>();
			for (int i = 0; i < poolSize; i++) {
				referencePool.Add(objectPool[i].GetComponent<T>());
			}
		}
	}

	/// <summary>
	/// Returns next Object From Pool
	/// </summary>
	/// <returns>Next object In pool regardless of state</returns>
	public T GetNextFromPool() {
		var objectFromPool = referencePool[nextPooledObject];
		nextPooledObject = (nextPooledObject + 1) % PoolSize;
		return objectFromPool;
	}
	public GameObject GetNextGOFromPool() { // TODO: Remove Gameobject as return type
		var objectFromPool = objectPool[nextPooledObject];
		nextPooledObject = (nextPooledObject + 1) % PoolSize;
		return objectFromPool;
	}

	/// <summary>
	/// Return Reference Type from next inactive gameobject
	/// </summary>
	/// <returns></returns>
	public T GetNewObject() {
		bool found = false;
		T newObject = referencePool[0];
		while (!found) {
			if (!objectPool[nextPooledObject].activeInHierarchy) {
				newObject = referencePool[nextPooledObject];
				found = true;
			}
			nextPooledObject = (nextPooledObject + 1) % PoolSize;
		}
		return newObject;
	}

	public void OnDisable ()
	{
		Debug.Break ();
		DestroyObject (GO);
	}
}
