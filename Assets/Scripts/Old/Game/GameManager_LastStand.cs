﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurnType {
	PLAYER,
	ALLY,
	OPPONENT,
	ENEMY,
	COUNT,
}

public class GameManager_LastStand : MonoBehaviour {

	public Grid Grid;
	public GridDisplay GridDisplay;

	public TurnType FirstTurn;
	private TurnType CurrentTurn;
	public bool IsPlayerTurn { get { return CurrentTurn == TurnType.PLAYER; } }
	public bool IsEnemyTurn { get { return CurrentTurn == TurnType.ENEMY; } }

	private int nextAlliesTurn = 0;
	private List<Actor> allyActors = new List<Actor>();

	private int nextEnemiesTurn = 0;
	private List<Actor> enemyActors = new List<Actor>();

	public Player player;

	private void Awake() {
		Grid.CreateGrid();
		Grid.OnAllyActorCreated += AddAllyActor;
		Grid.OnEnemyActorCreated += AddEnemyActor;

		// Set Up Player Actor
		player.Initialize(Grid, TakeNextTurn, EndTurn, RemoveAllyActor);
		Grid.PressPiece = player.PressPiece;
		Grid.ReleasePiece = player.ReleasePiece;
	}

	private void Start() {
		Grid.BeginFill();
		TakeNextTurn();
	}

	//Only Player and opponent call this directly
	public void EndTurn(TurnType type) {
		if (type == TurnType.PLAYER)
			GridDisplay.SetFillingLockDispayState (true);

		CurrentTurn = (TurnType)(((int)CurrentTurn + 1) % (int)TurnType.COUNT);
		TakeNextTurn();
	}

	public void TakeNextTurn() {
		if (CurrentTurn == TurnType.ALLY)
		{
			if (allyActors.Count > 0)
				StartCoroutine (DoAfterWait (NextTurn, 0.35f));
			else
				NextTurn ();
		}else if(CurrentTurn == TurnType.ENEMY)
		{
			if (enemyActors.Count > 0)
				StartCoroutine (DoAfterWait (NextTurn, 0.35f));
			else
				NextTurn ();
		}else if(CurrentTurn == TurnType.OPPONENT)
		{
			//TODO: Wait for opponent to make turn
			NextTurn ();
		}else if(CurrentTurn == TurnType.PLAYER)
		{
			StartCoroutine (DoAfterWait (NextTurn, 0.35f));
		}
		// TODO: Cape controll here
		//StartCoroutine(DoAfterWait(NextTurn, 0.35f));
	}

	public void NextTurn() {
		switch (CurrentTurn) {
			case TurnType.PLAYER:
				Debug.Log("PLAYER Turn");
				GridDisplay.SetFillingLockDispayState (false);
				player.TakeTurn();
				// Allow player Interactions
				break;

			case TurnType.ALLY:
				Debug.Log("Allies Turn");
				if (nextAlliesTurn < allyActors.Count) {
					////Debug.Log("Next Ally Takes Their Turn");
					allyActors[nextAlliesTurn].TakeTurn();
				}
				else {
					////Debug.Log("All Allies Have Taken Turns");
					nextAlliesTurn = 0;
					EndTurn(TurnType.ALLY);
				}
				break;

			case TurnType.OPPONENT:
				Debug.Log("Opponent Takes Their Turn");
				// Opponent AI Takes Turn or Spwans Enemies ( for single player games )
				////Debug.Log("Opponent Ends Thier Turn");
				EndTurn(TurnType.OPPONENT);
				break;

			case TurnType.ENEMY:
				Debug.Log("Enemy Turn");
				if (nextEnemiesTurn < enemyActors.Count) {
					////Debug.Log("Next Enemy Takes Their Turn");
					enemyActors[nextEnemiesTurn].TakeTurn();
				}
				else {
					////Debug.Log("All Enemies Have Taken Their Turns");
					nextEnemiesTurn = 0;
					EndTurn(TurnType.ENEMY);
				}
				break;
		}
	}

	//Allies
	public void AddAllyActor(Actor ally) {
		if (!allyActors.Contains(ally)) {
			allyActors.Add(ally);
			ally.Initialize(Grid, () => { nextAlliesTurn++; TakeNextTurn(); }, EndTurn, RemoveAllyActor);
		}
		else
			Debug.Log("Ally already added!");
	}
	public void RemoveAllyActor(Actor ally) {
		if (allyActors.Contains(ally)) {
			allyActors.RemoveAt(allyActors.IndexOf(ally));
			if(nextAlliesTurn > 0)
				nextAlliesTurn--;
		}
	}

	//Enemies
	public void AddEnemyActor(Actor enemy) {
		if (!enemyActors.Contains(enemy)) {
			enemyActors.Add(enemy);
			enemy.Initialize(Grid, ()=> { nextEnemiesTurn++; TakeNextTurn(); }, EndTurn, RemoveAllyActor);
		}
		else
			Debug.Log("Enemy already added!");
	}
	public void RemoveEnemyActor(Actor enemy) {
		if (enemyActors.Contains(enemy)) {
			enemyActors.RemoveAt(enemyActors.IndexOf(enemy));
			nextEnemiesTurn--;
		}
	}

	private IEnumerator DoAfterWait(Action DoAfter, float time) {
		yield return new WaitForSeconds(time);

		DoAfter();
	}
}
