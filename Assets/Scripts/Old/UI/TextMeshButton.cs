﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.Events;

public class TextMeshButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {

	[Header("Required")]
	public TextMeshProUGUI TextElement;

	[Header("Interaction Behavior")]
	public bool Interactable = true;
	public bool interactable {
		get {
			return Interactable;
		}
		set {
			Interactable = value;
			if (!Interactable) {
				SetColorDisabled();
			}
		}
	}
	public bool DisableAfterInteraction = false;
	[Space]
	public TextMeshButtonEvent OnClickEvent;

	[Header("Colors")]
	public Color EnabledColor = Color.white;
	public Color DisabledColor = new Color(1f,1f,1f, 0.5f);
	public Color PressedCollor = new Color(0.4f, 0.4f, 0.4f, 0.5f);

	private bool lastInteractable = true;
	private bool isPressed = false;
	private bool isValidPress = false;

	//////////////////////////////////// SET COLORS ////////////////////////////////////

	private void SetColorDisabled() {
		TextElement.color = DisabledColor;
	}
	private void SetColorEnabled() {
		TextElement.color = EnabledColor;
	}
	private void SetColorPressed() {
		TextElement.color = PressedCollor;
	}

	////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////// SET STATES ////////////////////////////////////

	public void OnPointerDown(PointerEventData eventData) {
		if (Interactable) {
			isPressed = true;
			isValidPress = true;
			SetColorPressed();
		}
	}

	public void OnPointerUp(PointerEventData eventData) {
		if (isValidPress) {
			isPressed = false;

			//Trigger Event
			if (OnClickEvent != null)
				OnClickEvent.Invoke();

			if (DisableAfterInteraction) {
				Interactable = false;
				SetColorDisabled();
			}
			else
				SetColorEnabled();
		}
		else {
			SetColorEnabled();
			isPressed = false;
		}
	}

	public void OnPointerEnter(PointerEventData eventData) {
		if (isPressed) {
			SetColorPressed();
			isValidPress = true;
		}
	}

	public void OnPointerExit(PointerEventData eventData) {
		if (isPressed) {
			SetColorEnabled();
			isValidPress = false;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////
}

[System.Serializable]
public class TextMeshButtonEvent : UnityEvent { }