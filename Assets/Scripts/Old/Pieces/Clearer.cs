﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoWar
{
	public abstract class Clearer : MonoBehaviour
	{

		public Action PieceCleared;

		public int ClearCount { get; protected set; }

		public abstract bool Clear ();

		public void ResetClearer ()
		{
			ClearCount = 0;
		}
	}
}