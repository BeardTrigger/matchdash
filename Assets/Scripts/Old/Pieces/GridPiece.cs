﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridPiece : Piece {
	
	protected Action<GridPiece> mouseDown;
	protected Action<Directions> mouseUp;

	private Vector3 onPressPosition;
	private bool hasPressed = false;
	private bool hasLeft = false;

	public virtual void PreInit() {
		MovablePiece = GetComponent<Mover> ();
		//ClearablePiece = GetComponent<Clearer> ();
	}

	public virtual void Set(int x, int y, Action<GridPiece> mouseDown, Action<Directions> mouseUp) {
		X = x;
		Y = y;

		transform.position = Grid.GetGridWorldPosition(x, y);

		this.mouseDown = mouseDown;
		this.mouseUp = mouseUp;

		gameObject.SetActive(true);
	}

	protected void OnMouseDown() {
		hasPressed = true;
		onPressPosition = Input.mousePosition;
		if (mouseDown != null)
			mouseDown(this);
	}

	protected void OnMouseExit() {
		if (hasPressed)
			hasLeft = true;
	}

	protected void OnMouseUp() {
		if (!hasPressed || !hasLeft) {
			hasPressed = false;
			hasLeft = false;
			return;
		}

		var dir = (Input.mousePosition - onPressPosition).normalized;
		Directions directionOfSwipe;
		// Check if hrozontal of verticaly dominant
		if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y)) {
			// Horizontal, So check left or right
			if (dir.x > 0f) {
				// Right
				directionOfSwipe = Directions.RIGHT;
			}
			else {
				// Left
				directionOfSwipe = Directions.LEFT;
			}
		}
		else {
			// Vertical, so check Up or Down
			if (dir.y > 0) {
				// Up
				directionOfSwipe = Directions.UP;
			}
			else {
				// Down
				directionOfSwipe = Directions.DOWN;
			}
		}

		hasPressed = false;
		hasLeft = false;
		if (mouseUp != null)
			mouseUp(directionOfSwipe);
	}
}