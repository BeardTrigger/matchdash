﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridModelPiece : Piece
{
	public bool IsSpawnedPiece = false;

	protected override void Awake ()
	{
		MovablePiece = new MovableModelPiece ();
		//MovablePiece.piece = this;

		//ClearablePiece = new ClearableModelPiece ();
	}

	public GridModelPiece (int x, int y, PieceType type, bool isMatchable, bool isMovable, bool isClearable, int clearThreshold, bool SpawnedPiece)
	{
		InitializeGridModel (x, y, type, isMatchable, isMovable, isClearable, clearThreshold, SpawnedPiece);
	}

	public void InitializeGridModel (int x, int y, PieceType type, bool isMatchable, bool isMovable, bool isClearable, int clearThreshold, bool SpawnedPiece)
	{
		X = x;
		Y = y;
		Type = type;
		IsMatchable = isMatchable;
		IsMovable = isMovable;
		IsClearable = isClearable;
		IsSpawnedPiece = SpawnedPiece;
		//if (IsClearable)
		//	ClearablePiece.ClearThresholdCount = clearThreshold;
	}
	public void InitializeGridModel (GridModelPiece modelPiece, int x, int y)
	{
		X = x;
		Y = y;
		Type = modelPiece.Type;
		IsMatchable = modelPiece.IsMatchable;
		IsMovable = modelPiece.IsMovable;
		IsClearable = modelPiece.IsClearable;
		IsSpawnedPiece = modelPiece.IsSpawnedPiece;
		//if (IsClearable)
		//	ClearablePiece.ClearThresholdCount = modelPiece.ClearablePiece.ClearThresholdCount;

	}
	
}
