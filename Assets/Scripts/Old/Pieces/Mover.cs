﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Mover : MonoBehaviour {

	abstract public void Move (int newX, int newY, float time, Action onMoveFinished);
	abstract public void Move (Vector3 position, float time, Action onMoveFinished);
}
