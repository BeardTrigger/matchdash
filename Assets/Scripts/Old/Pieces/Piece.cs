﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour {

	public int X, Y;
	public PieceType Type;
	public bool IsMatchable;
	public bool IsMovable;
	public Mover MovablePiece;
	public bool IsClearable;
	//public Clearer ClearablePiece;

	protected virtual void Awake ()
	{
		MovablePiece = GetComponent<Mover> ();
		//ClearablePiece = GetComponent<Clearer> ();
	}
}
