﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team
{
	Neautral,
	Darkness, // Gravity -1: UP;
	Light, //Gravity 1: Down
	Count,
	None,
}

public class GridTile {

	public int X, Y;

	public Team Team;

	public int ClaimThreshold = 2;
	public int GravityMultypier { get { if (Team == Team.Darkness)
				return -1;
			else if (Team == Team.Light)
				return 1;
			else
				return 0;
		} }

	protected Team currentlyClaimingTeam = Team.None;
	protected int currentlyClaimingCount = 0;

	public GridTile(int x, int y, Team team)
	{
		X = x;
		Y = y;
		Team = team;
	}

	public void MakeClaim(Team teamMakinClaim)
	{
		if(currentlyClaimingTeam == Team.Neautral || teamMakinClaim == currentlyClaimingTeam)
		{
			currentlyClaimingCount++;
			if (currentlyClaimingCount >= ClaimThreshold)
				ChangeOwners (teamMakinClaim);
		} else
		{
			//Make Tile Neutral
			ChangeOwners (Team.Neautral);
		}
	}

	protected void ChangeOwners (Team newTeam)
	{
		Team = newTeam;
		currentlyClaimingCount = 0;
		currentlyClaimingTeam = Team.None;
	}
}
