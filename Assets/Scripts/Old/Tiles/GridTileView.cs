﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTileView : MonoBehaviour {

	public int X, Y;

	public SpriteRenderer Sprite;
	public bool TileSet;
	private TileColor TileColorSet;
	private TileColor TileColor;
	private Color NeutralColor;

	private bool HasBeenSelected = false;

	private Action<int,int> OnHasBeenSelected;
	private Action BackToNeutral;

	private bool Placing = false;



	private void Awake() {
		NeutralColor = Sprite.color;
	}

	private void OnMouseUpAsButton() {
		if (Placing) {
			HasBeenSelected = true;

			if (BackToNeutral != null)
				BackToNeutral();
			BackToNeutral = null;

			if (OnHasBeenSelected != null)
				OnHasBeenSelected(X, Y);
			OnHasBeenSelected = null;

		}
	}

	private void OnMouseEnter() {
		if (Placing)
			Sprite.color = TileColor.HighlightedColor;


	}

	private void OnMouseExit() {
		if (Placing) {
			if (!HasBeenSelected)
				Sprite.color = TileColor.ActiveColor;
		}

		if (TileSet)
			Sprite.color = TileColorSet.SetColor;
	}

	public void ApplyColor(TileColor color, Action<int, int> OnSelected, Action GoBackToNeutral) {
		TileColor = color;
		Sprite.color = TileColor.ActiveColor;

		HasBeenSelected = false;

		OnHasBeenSelected = OnSelected;
		BackToNeutral = GoBackToNeutral;

		Placing = true;
	}

	public void ApplyColor(TileColor color) {
		TileColor = color;
		Sprite.color = TileColor.ActiveColor;
	}

	public void ToNeutral(bool clear = false) {
		Placing = false;

		if (clear && TileSet) {
			TileSet = false;
		}

		if (TileSet)
			Sprite.color = TileColorSet.SetColor;
		else
		{
			if (Sprite == null || NeutralColor == null)
				Debug.Break();
			Sprite.color = NeutralColor;
		}

	}

	public void SetTileColor(TileColor color) {
		TileSet = true;
		TileColorSet = color;
		Sprite.color = TileColorSet.SetColor;
	}
}
