﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridFillAI : MonoBehaviour {


	private GridModelPiece[,] GridModelPiece;

	private int XDim, YDim;
	private bool inverse = false;

	private List<Queue<PieceType>> ColumnQueues;


	public void DoPredictionProccess(GridPiece[,] gridPieces, int XDim, int YDim, bool inverse, Action<List<Queue<PieceType>>> onProcessComplete) {
		////Debug.Log("DoPredictionProccess");
		SnapshotGrid(gridPieces, XDim, YDim, inverse);
		StabilizeModel();
		PopulateQueues();
		onProcessComplete(ColumnQueues);
	}

	/// <summary>
	/// Makes Model copy of grid to tick forward and pick new tiles that will not cause waterfall matching
	/// </summary>
	/// <param name="gridPieces"></param>
	/// <param name="XDim"></param>
	/// <param name="YDim"></param>
	/// <param name="inverse"></param>
	private void SnapshotGrid(GridPiece[,] gridPieces, int XDim, int YDim, bool inverse) {

		////Debug.Log("Taking Snapshot");

		if (GridModelPiece == null || GridModelPiece.Length != XDim * YDim)
			GridModelPiece = new GridModelPiece[XDim, YDim];

		for (int x = 0; x < XDim; x++) {
			for (int y = 0; y < YDim; y++) {

				GridPiece piece = gridPieces[x,y];
				int cleartheshold = 0;

				//if (piece.IsClearable)
				//	cleartheshold = piece.ClearablePiece.ClearThresholdCount;

				if (GridModelPiece[x, y] == null)
					GridModelPiece[x, y] = new GridModelPiece(x, y, piece.Type, piece.IsMatchable, piece.IsMovable, piece.IsClearable, cleartheshold, false);
				else
					GridModelPiece[x, y].InitializeGridModel (x, y, piece.Type, piece.IsMatchable, piece.IsMovable, piece.IsClearable, cleartheshold, false);
				
			}
		}

		this.XDim = XDim;
		this.YDim = YDim;
		this.inverse = inverse;

	}

	private void StabilizeModel() {
		do {
			while (MoveModelPieces()) {
				inverse = !inverse;
			}
		} while (ClearAllMatches());
	}

	private bool MoveModelPieces() {
		////Debug.Log("MoveModelPieces");
		bool movedModel = false;

		//Move Pieces Down the Grid
		for (int y = 1; y < YDim; y++) {
			for (int xLoop = 0; xLoop < XDim; xLoop++) {
				int x = xLoop;

				GridModelPiece piece = GridModelPiece[x, y];

				if (piece.IsMovable) {
					GridModelPiece pieceBelow = GridModelPiece[x, y - 1];

					if (pieceBelow.Type == PieceType.EMPTY) {
						GridModelPiece[x, y - 1].InitializeGridModel (piece, x, y - 1);
						GridModelPiece[x, y].InitializeGridModel (x, y, PieceType.EMPTY, false, false, false, 0, false);

						movedModel = true;
					}
					else /*if (Grid.GoAround(pieceBelow.Type))*/ {
						// Try to move diagonaly

						for (int diag = -1; diag <= 1; diag++) {
							if (diag == 0)
								continue;

							int diagX = x + diag;
							if (inverse) diagX = x - diag;

							if (diagX < 0 || diagX >= XDim)
								continue;

							GridModelPiece diagonalPiece = GridModelPiece[diagX, y - 1];

							if (diagonalPiece.Type == PieceType.EMPTY) {
								bool hasPieceAbove = true;
								// Check for pieces above
								for (int aboveY = y; aboveY < YDim; aboveY++) {
									GridModelPiece pieceAboveDiagonal = GridModelPiece[diagX, aboveY];

									if (pieceAboveDiagonal.Type == PieceType.EMPTY)
										continue;
									else if (pieceAboveDiagonal.IsMovable)
										break;
									else if(aboveY == y && Grid.GoAround(pieceAboveDiagonal.Type) && Grid.GoAround(pieceBelow.Type))
										break;

									hasPieceAbove = false;
								}
								if (!hasPieceAbove) {
									GridModelPiece[diagX, y - 1].InitializeGridModel (piece, diagX, y - 1);
									GridModelPiece[x, y].InitializeGridModel (x, y, PieceType.EMPTY, false, false, false, 0, false);

									movedModel = true;
									break;
								}
							}
						}
					}
				}
			}
		}

		bool spawnedNew = SpawnNewPieces();
		if (spawnedNew)
			movedModel = true;

		////Debug.Log("movedModel" + movedModel);
		return movedModel;
	}

	private bool SpawnNewPieces() {
		bool spawned = false;
		for (int x = 0; x < XDim; x++) {
			if (GridModelPiece[x, YDim - 1].Type == PieceType.EMPTY) {
				PieceType newPiece = RandomizePiece(x, YDim - 1);

				GridModelPiece[x, YDim - 1].InitializeGridModel (x, YDim - 1, newPiece, true, true, true, 1, true);

				spawned = true;
			}
		}
		return spawned;
	}

	private PieceType RandomizePiece(int x, int y) {
		PieceType newPiece = PieceType.ANY;
		do {
			newPiece = (PieceType)UnityEngine.Random.Range(0, (int)PieceType.NUM);
		} while (!ValidatePieceType(x, YDim - 1, newPiece));

		return newPiece;
	}

	private bool ValidateMatch(List<GridModelPiece> matches) {
		bool valid = false;
		for (int i = 0; i < matches.Count; i++) {
			if (matches[i].IsSpawnedPiece) {
				PieceType newPiece = RandomizePiece(matches[i].X, matches[i].Y);

				GridModelPiece[matches[i].X, matches[i].Y].InitializeGridModel (matches[i].X, matches[i].Y, newPiece, true, true, true, 1, true);
				return valid;
			}
		}
		return true;
	}

	List<GridModelPiece> PiecesToClear = new List<GridModelPiece>();
	private bool ClearAllMatches() {

		////Debug.Log("ClearAllMatches");
		bool needsFill = false;

		if (PiecesToClear.Count > 0)
			PiecesToClear.Clear();

		int checkOffset = 3;
		int xCheck = 2;
		int yCheck = 2;

		for (int y = 0; y < YDim; y++) {
			for (int x = 0; x < XDim; x++) {

				if (x == xCheck && GridModelPiece[x, y].IsMatchable) {

					List<GridModelPiece> matching = DeepMatchCount(GridModelPiece[x, y]);
					
					if (matching.Count >= 3) {
						if (!ValidateMatch(matching)) {//Because part of the match is a spawned piece
							return true;
						}
						for (int j = 0; j < matching.Count; j++) {
							if (!PiecesToClear.Contains(matching[j]))
								PiecesToClear.Add(matching[j]);

							var adjacentObstacles = GetAdjacent(x,y);
							for (int k = 0; k < adjacentObstacles.Length; k++) {
								if (adjacentObstacles[k] != null) {
									//if (adjacentObstacles [k].ClearablePiece.Clear() && !PiecesToClear.Contains (adjacentObstacles [k]))
									//	PiecesToClear.Add(adjacentObstacles[k]);
								}
							}
						}
					}
					xCheck += checkOffset;
				}

				if (y == yCheck && GridModelPiece[x, y].IsMatchable) {
					List<GridModelPiece> matching = DeepMatchCount(GridModelPiece[x, y]);

					if (matching.Count >= 3) {
						if (!ValidateMatch(matching)) {//Because part of the match is a spawned piece
							return true;
						}
						for (int j = 0; j < matching.Count; j++) {
							if (!PiecesToClear.Contains(matching[j]))
								PiecesToClear.Add(matching[j]);

							var adjacentObstacles = GetAdjacent(x,y);
							for (int k = 0; k < adjacentObstacles.Length; k++) {
								if (adjacentObstacles[k] != null) {
									//if (adjacentObstacles [k].ClearablePiece.Clear () && !PiecesToClear.Contains (adjacentObstacles [k]))
									//	PiecesToClear.Add (adjacentObstacles [k]);
								}
							}
						}
					}
				}
			}
			xCheck = 2;

			if (y == yCheck)
				yCheck += checkOffset;
		}

		if (PiecesToClear.Count >= 3) {
			ClearListPiecesNoFill(PiecesToClear);
			needsFill = true;
		}

		////Debug.Log("ClearAllMatches" + needsFill);
		return needsFill;
	}

	List<GridModelPiece> matchingPieces = new List<GridModelPiece>();
	List<GridModelPiece> matchingHorizontalPieces = new List<GridModelPiece>();
	List<GridModelPiece> matchingVerticalPieces = new List<GridModelPiece>();
	private List<GridModelPiece> DeepMatchCount(GridModelPiece piece) {
		// Add piece cos its in da mach yo
		matchingPieces.Clear();
		matchingHorizontalPieces.Clear();
		matchingVerticalPieces.Clear();

		int x = piece.X;
		int y = piece.Y;
		// Horizontal First
		matchingHorizontalPieces.Add(piece);
		for (int dir = 0; dir < 2; dir++) {

			for (int i = 0; i < XDim; i++) {
				x = dir == 0 ? x -= 1 : x += 1; // Direction ? left : right

				if (x < 0 || x >= XDim)
					break; // Check Bounds

				if (GridModelPiece[x, y].Type == piece.Type)
					matchingHorizontalPieces.Add(GridModelPiece[x, y]);
				else
					break;
			}
			x = piece.X;
		}
		if (matchingHorizontalPieces.Count >= 3) {
			// Add successful pices to matching list
			for (int i = 0; i < matchingHorizontalPieces.Count; i++) {
				if (!matchingPieces.Contains(matchingHorizontalPieces[i]))
					matchingPieces.Add(matchingHorizontalPieces[i]);
			}

			// Check matching pieces for Vertical matches
			for (int j = 0; j < matchingHorizontalPieces.Count; j++) {
				x = matchingHorizontalPieces[j].X;
				y = matchingHorizontalPieces[j].Y;

				for (int dir = 0; dir < 2; dir++) {

					for (int i = 0; i < YDim; i++) {
						y = dir == 0 ? y -= 1 : y += 1; // Direction ? down : up

						if (y < 0 || y >= YDim)
							break; // Check Bounds

						if (GridModelPiece[x, y].Type == piece.Type)
							matchingVerticalPieces.Add(GridModelPiece[x, y]);
						else
							break;
					}
					y = piece.Y;
				}
				//if (matchingVerticalPieces.Count >= 2) {
				for (int i = 0; i < matchingVerticalPieces.Count; i++) {
					if (!matchingPieces.Contains(matchingVerticalPieces[i]))
						matchingPieces.Add(matchingVerticalPieces[i]);
				}
				//break;
				//}
				//else {
				matchingVerticalPieces.Clear();
				//}
			}

			// Found our match so return
			return matchingPieces;
		}

		// Reset For Vertical
		matchingHorizontalPieces.Clear();
		matchingVerticalPieces.Clear();

		x = piece.X;
		y = piece.Y;

		// Vertical
		matchingVerticalPieces.Add(piece);
		for (int dir = 0; dir < 2; dir++) {

			for (int i = 1; i < YDim; i++) {
				y = dir == 0 ? y -= 1 : y += 1; // Direction ? down : up

				if (y < 0 || y >= YDim)
					break; // Check Bounds

				if (GridModelPiece[x, y].Type == piece.Type)
					matchingVerticalPieces.Add(GridModelPiece[x, y]);
				else
					break;
			}
			y = piece.Y;
		}
		if (matchingVerticalPieces.Count >= 3) {
			for (int i = 0; i < matchingVerticalPieces.Count; i++) {
				if (!matchingPieces.Contains(matchingVerticalPieces[i]))
					matchingPieces.Add(matchingVerticalPieces[i]);
			}
			////Check this match for Vertical Matches
			for (int j = 0; j < matchingVerticalPieces.Count; j++) {
				x = matchingVerticalPieces[j].X;
				y = matchingVerticalPieces[j].Y;

				for (int dir = 0; dir < 2; dir++) {

					for (int i = 0; i < YDim; i++) {
						x = dir == 0 ? x -= 1 : x += 1; // Direction ? left : rigth

						if (x < 0 || x >= XDim)
							break; // Check Bounds

						if (GridModelPiece[x, y].Type == piece.Type)
							matchingHorizontalPieces.Add(GridModelPiece[x, y]);
						else
							break;
					}
					x = piece.X;
				}
				//if (matchingHorizontalPieces.Count >= 2) {
				for (int i = 0; i < matchingHorizontalPieces.Count; i++) {
					if (!matchingPieces.Contains(matchingHorizontalPieces[i]))
						matchingPieces.Add(matchingHorizontalPieces[i]);
				}
				//break;
				//}
				//else {
				matchingHorizontalPieces.Clear();
				//}
			}

			// Found our match so returnd
			return matchingPieces;
		}

		if (matchingPieces.Count < 3) {
			matchingPieces.Clear();
		}
		// Satisfies Function but will never be hit
		////Debug.LogError("HOW THE FUCK DID YOU GET DOWN HERE?");
		return matchingPieces;
	}

	GridModelPiece[] adjacent = new GridModelPiece[4];
	private GridModelPiece[] GetAdjacent(int x, int y) {
		for (int i = 0; i < 4; i++) {
			adjacent[i] = null;
		}

		if (CheckBounds(x, y + 1)) { // UP
			if (GridModelPiece[x, y + 1].Type == PieceType.OBSTACLE) //TODO: || other proximity clear types
				adjacent[0] = GridModelPiece[x, y + 1];
		}
		if (CheckBounds(x, y - 1)) {// Down
			if (GridModelPiece[x, y - 1].Type == PieceType.OBSTACLE)
				adjacent[1] = GridModelPiece[x, y - 1];
		}
		if (CheckBounds(x - 1, y)) {// Left
			if (GridModelPiece[x - 1, y].Type == PieceType.OBSTACLE)
				adjacent[2] = GridModelPiece[x - 1, y];
		}
		if (CheckBounds(x + 1, y)) {// Right
			if (GridModelPiece[x + 1, y].Type == PieceType.OBSTACLE)
				adjacent[3] = GridModelPiece[x + 1, y];
		}
		return adjacent;
	}

	private bool CheckBounds(int x, int y) {
		if (x < 0 || x >= XDim || y < 0 || y >= YDim)
			return false;
		else
			return true;
	}

	private void ClearListPiecesNoFill(List<GridModelPiece> list) {
		for (int i = 0; i < list.Count; i++) {
			////Debug.Log(string.Format("GRID CLEARING {0}:{1}, {2}", list[i].X, list[i].Y, list[i].Type));
			GridModelPiece[list[i].X, list[i].Y].InitializeGridModel (list[i].X, list[i].Y, PieceType.EMPTY, false, false, false, 0, false);
		}
	}

	private void PopulateQueues() {
		////Debug.Log("PopulateQueues");
		if (ColumnQueues == null) {
			ColumnQueues = new List<Queue<PieceType>>();
		}
		for (int x = 0; x < XDim; x++) {
			if (ColumnQueues.Count == x)
				ColumnQueues.Add(new Queue<PieceType>());

			// Find empty in column and fill
			for (int y = 0; y < YDim; y++) {
				if (GridModelPiece[x, y].IsSpawnedPiece) {
					//PieceType newPiece = PieceType.ANY;
					//do {
					//	newPiece = (PieceType)UnityEngine.Random.Range(0, (int)PieceType.NUM);
					//} while (!ValidatePieceType(x, y, newPiece));
					////Debug.Log("Queue :" + x + "Type: " + newPiece.ToString() + " at: " + x + " , " + y);
					//GridModelPiece[x, y].Type = newPiece;
					ColumnQueues[x].Enqueue(GridModelPiece[x, y].Type);
				}
			}
		}
	}

	private bool ValidatePieceType(int x, int y, PieceType type) {

		if (QuickMatchCheckHorizontal(x, y, type) || QuickMatchCheckVertical(x, y, type))
			return false;

		return true;
	}

	private bool QuickMatchCheckHorizontal(int CheckX, int y, PieceType type) {

		int consecutiveCount = 0;

		for (int dir = 0; dir < 2; dir++) {

			for (int xOffset = 1; xOffset < XDim; xOffset++) {
				int x = dir == 0 ? CheckX - xOffset : CheckX + xOffset; // Direction ? left : right

				if (x < 0 || x >= XDim)
					break; // Check Bounds

				if (GridModelPiece[x, y].Type == type)
					consecutiveCount++;
				else
					break;
			}
			if (consecutiveCount >= 2)
				return true;
		}
		return false;
	}

	private bool QuickMatchCheckVertical(int x, int CheckY, PieceType type) {

		int consecutiveCount = 0;

		for (int dir = 0; dir < 2; dir++) {

			for (int yOffset = 1; yOffset < YDim; yOffset++) {
				int y = dir == 0 ? CheckY - yOffset : CheckY + yOffset; // Direction ? down : up

				if (y < 0 || y >= YDim)
					break; // Check Bounds

				if (GridModelPiece[x, y].Type == type)
					consecutiveCount++;
				else
					break;
			}
			if (consecutiveCount >= 2)
				return true;
		}
		return false;
	}
	Vector3 size = new Vector3(0.5f, 0.5f, 0.01f);
	private void OnDrawGizmos() {
		for (int x = 0; x < XDim; x++) {
			for (int y = 0; y < YDim; y++) {
				switch (GridModelPiece[x, y].Type) {
					case PieceType.RED:
						Gizmos.color = Color.red;
						break;
					case PieceType.YELLOW:
						Gizmos.color = Color.yellow;
						break;
					case PieceType.BLUE:
						Gizmos.color = Color.blue;
						break;
					case PieceType.BLACK:
						Gizmos.color = Color.black;
						break;
					case PieceType.GREEN:
						Gizmos.color = Color.green;
						break;
					case PieceType.PINK:
						Gizmos.color = Color.magenta;
						break;
					case PieceType.EMPTY:
						Gizmos.color = Color.white;
						break;
				}

				Gizmos.DrawCube(Grid.GetGridWorldPosition(x, y), size);
			}
		}
	}
}
