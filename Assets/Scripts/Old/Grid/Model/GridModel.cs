﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MatchDash.Model
{
	public enum TileType
	{
		NORMAL,
		FIRE,
		ICE,
		LIGHTNING,
		CREEP,
		COUNT,
	}
	public enum Team
	{
		NEUTRAL,
		DARKNESS, // Gravity -1: UP;
		LIGHT, //Gravity 1: Down
		COUNT,
		NONE,
	}
	public enum PieceType
	{
		RED,
		YELLOW,
		BLUE,
		BLACK,
		GREEN,
		PINK,
		NUM,
		EMPTY,
		OBSTACLE,
		ALLY,
		ANY,
		COUNT,
	}

	public class GridModel
	{

		[Header ("Properties")]
		[SerializeField]
		protected int XDim;
		[SerializeField]
		protected int YDim;

		[Header ("Tiles")]
		[SerializeField]
		protected GridTile [,] gridTiles;

		[Header ("Pieces")]
		[SerializeField]
		protected Piece [,] gridModelPieces;
		[SerializeField]
		protected Piece [,] gridPlayPieces;

		public void InitializeGridModel (int xDim, int yDim)
		{
			XDim = xDim;
			YDim = yDim;
		}

	}	  
}