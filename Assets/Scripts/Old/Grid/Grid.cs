﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Controller
public enum Directions {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	HORIZONTAL,
	VERTICAL,
}

//Controller
public enum PieceType {
	RED,
	YELLOW,
	BLUE,
	BLACK,
	GREEN,
	PINK,
	NUM,
	EMPTY,
	OBSTACLE,
	ALLY,
	ANY,
	COUNT,
}

public class Grid : MonoBehaviour {


	[Header("Properties")]
	public int XDim;
	public int YDim;

	//View
	public float HeightOffset = 0.5f;
	public float PositionScalar = 1f;
	[Space]
	public float FillTime = 0.2f;
	public Directions FillDirection;

	//////////////////////////////		Tiles
	public enum TileType {
		NORMAL,
		FIRE,
		ICE,
		LIGHTNING,
		CREEP,
		COUNT,
	}
	[System.Serializable]
	public struct TileDisplay {
		public TileType Type;
		public GameObject TilePrefab;
	}
	[Header("Tiles")]
	public TileDisplay[] TilesDisplay;
	public Transform TilesParent;

	private Dictionary<TileType, GameObject> tilePrefabDict;

	private GridTile [,] gridTilesModel;
	private GridTileView[,] gridTilesDisplay;
	private Dictionary<TileType, Pool<GridTileView>> tilePools;

	///////////////////////////////////		Pieces
	[System.Serializable]
	public struct PieceTypes {
		public PieceType Type;
		public GameObject PiecePrefab;
	}
	[Header("Pieces")]
	public PieceTypes [] Pieces;
	public GameObject EmptyPiece;
	public Transform PiecesParent;
	public Transform AlliesParent;

	private Dictionary<PieceType, GameObject> piecePrefabDict;

	private Piece[,] grid;
	private Piece [,] gridModel;
	private Dictionary<PieceType, Pool<GridPiece>> piecePools;
	private Piece emptyPiece;
	private List<Queue<Piece>> FillQues;
	private List<Queue<PieceType>> ColumnQueuesTop;
	private List<Queue<PieceType>> ColumnQueuesBottom;
	private int[] poolsIndex;

	public int NumberOnPieceTypes { get { return (int)PieceType.NUM; } }

	///////////////////////////////////////// Grid Display
	public GridDisplay gridDisplay;

	///////////////////////////////////////// Other Variables
	public GridFillAI gridFillAI;

	private bool inverse = false;

	public Action<GridPiece> PressPiece;
	public Action<Directions> ReleasePiece;

	public delegate void GridCallback();
	public event GridCallback OnSwapMatched;
	public event GridCallback OnFillCompete;

	public delegate void GridActorCreate(Actor actor);
	public event GridActorCreate OnAllyActorCreated;
	public event GridActorCreate OnEnemyActorCreated;

	private bool isFilling = false;
	private bool initialFill = true;


	public bool IsIntractable { get { return !isFilling || !NoSwappingWhenFilling; } }
	///////////////////////////////////////// Service Getter
	private static Grid Instance;

	[Header("//Debug")]
	public bool FillAfterDelay = false;
	public bool StartFill = false;
	public float FillDelay;
	[Header("Rules")]
	public bool HasToBeMatchToSwap = true;
	public bool NoSwappingWhenFilling = false;

	[Header("Powers")]
	public Power[] Powers;

	private void Awake() {
		Instance = this;
		gridDisplay = GetComponent<GridDisplay>();
		//gridFillAI = new GridFillAI();
	}


	// Creators
	public void CreateGrid() {
		CreateTiles();
		CreatePieces();

		gridDisplay.Initialize(gridTilesDisplay);

		CreateFillQueues();
	}

	private void CreateTiles() {
		//Construct Dictionary of Tiles
		tilePrefabDict = new Dictionary<TileType, GameObject>();
		tilePools = new Dictionary<TileType, Pool<GridTileView>>();
		gridTilesDisplay = new GridTileView[XDim, YDim];
		gridTilesModel = new GridTile [XDim, YDim];

		for (int i = 0; i < TilesDisplay.Length; i++) {
			if (!tilePrefabDict.ContainsKey(TilesDisplay[i].Type)) {
				tilePrefabDict.Add(TilesDisplay[i].Type, TilesDisplay[i].TilePrefab);

				//Generate Pool of TileType
				tilePools.Add(TilesDisplay[i].Type, new Pool<GridTileView>(tilePrefabDict[TilesDisplay[i].Type], XDim * YDim/*, tilesparent*/));
			}
		}
		//Set Up Normal Starting Board
		for (int i = 0; i < tilePools[TileType.NORMAL].PoolSize; i++) {
			int x = i % XDim;
			int y = Mathf.FloorToInt(i / XDim);
			GameObject NewTile = tilePools[TileType.NORMAL].GetNextGOFromPool();
			gridTilesModel [x, y] = NewTile.GetComponent<GridTile> ();
			if (y < YDim / 2)
				gridTilesModel [x, y].Team = Team.Darkness;
			else
				gridTilesModel [x, y].Team = Team.Light;

			gridTilesDisplay [x, y] = NewTile.GetComponent<GridTileView>();
			gridTilesDisplay[x, y].X = x;
			gridTilesDisplay[x, y].Y = y;
			//NewTile.name = string.Format("{0}({1},{2})", TileType.NORMAL.ToString(), x.ToString(), y.ToString());
			NewTile.transform.position = GetWorldPosition(x, y);
			NewTile.SetActive(true);
		}
	}
	private void CreatePieces() {
		//Construct Dictionary of Pieces
		piecePrefabDict = new Dictionary<PieceType, GameObject>();
		piecePools = new Dictionary<PieceType, Pool<GridPiece>>();
		grid = new GridPiece[XDim, YDim];
		gridModel = new GridModelPiece [XDim, YDim];
		poolsIndex = new int[(int)PieceType.COUNT];

		for (int i = 0; i < Pieces.Length; i++) {
			if (!piecePrefabDict.ContainsKey(Pieces[i].Type)) {
				piecePrefabDict.Add(Pieces[i].Type, Pieces[i].PiecePrefab);

				Power power = null;
				for (int j = 0; j < Powers.Length; j++) {
					if (Pieces[i].Type == Powers[j].Type)
						power = Powers[j];
				}


				Transform parent = PiecesParent;
				//Generate Pools of Pieces
				if (Pieces[i].Type == PieceType.ALLY)
					parent = AlliesParent;

				//Generate Pools of Pieces
				piecePools.Add(Pieces[i].Type, new Pool<GridPiece>(piecePrefabDict[Pieces[i].Type], XDim * YDim/*, parent*/));

				for (int j = 0; j < piecePools[Pieces[i].Type].PoolSize; j++) {
					var newPiece = piecePools[Pieces[i].Type].GetNextFromPool();
					//newPiece.PreInit();

					// Setup Observer for Pieces
					//if (power != null)
					//	newPiece.ClearablePiece.PieceCleared += power.PieceDestroyed;
				}
			}
		}

		emptyPiece = Instantiate(EmptyPiece, PiecesParent).GetComponent<GridPiece>();
		//Set Up Board Full of Empty Pieces
		for (int i = 0; i < XDim * YDim; i++) {
			int x = i % XDim;
			int y = Mathf.FloorToInt(i / XDim);
			grid[x, y] = emptyPiece;
		}
	}
	private void CreateFillQueues() {
		FillQues = new List<Queue<Piece>>();
		for (int x = 0; x < XDim; x++) {
			FillQues.Add(new Queue<Piece>());
		}
	}


	// Fill & Spawn
	public void BeginFill() {
		////Debug.Log("BeginFill");
		//gridFillAI.DoPredictionProccess(grid, XDim, YDim, inverse, FillPieceQueues);
	}
	private void FillPieceQueues(List<Queue<PieceType>> queues) {
		// Flush Current Cue
		for (int i = 0; i < FillQues.Count; i++) {
			if (FillQues[i].Count > 0)
				FillQues[i].Clear();
		}
		for (int x = 0; x < XDim; x++) {
			while (queues[x].Count > 0) {
				FillQues[x].Enqueue(EnableNewPieceLite(queues[x].Dequeue()));
			}
		}
		StartCoroutine(Fill());
	}
	private IEnumerator Fill() {
		if (!isFilling) {

			if (FillAfterDelay) {
				yield return new WaitForSeconds(FillDelay);
				StartFill = true;
				FillAfterDelay = false;
			}

			while (!StartFill) {
				yield return null;
			}

			//if(initialFill)
			//	AddNewPiecesToGrid();

			do {
				while (FillGrid()) {
					inverse = !inverse;
					yield return new WaitForSeconds(FillTime);
				}
			} while (ClearAllMatches());

			isFilling = false;

			//if (NoSwappingWhenFilling)
			//	FillingLock.SetActive(false);

			if (initialFill)
			{
				initialFill = false;
				gridDisplay.SetFillingLockDispayState (false);
			}else {
				if (OnFillCompete != null)
					OnFillCompete();
			}
		}
	}
	private bool FillGrid() {
		//if (NoSwappingWhenFilling)
		//	FillingLock.SetActive(true);

		isFilling = true;
		return MovePieces();
	}

	protected bool LoopCondition (Team team, int index)
	{
		if (team == Team.Light)
		{
			if (index < YDim)
				return true;
			else
				return false;
		} else if (team == Team.Darkness)
		{
			if (index >= 0)
				return true;
			else
				return false;
		}
		return false;
	}
	private bool MovePieces() {
		bool movedPiece = false;
		//Debug.Log("MovePiecesDownBoard");
		//Move Pieces Down the Grid

		for (int y = 1; y < YDim; y++) {
			for (int xLoop = 0; xLoop < XDim; xLoop++) {
				int x = xLoop;

				Piece piece = grid[x, y];

				if (piece.IsMovable) {
					int directionalOffset = gridTilesModel [x, y].GravityMultypier;
					Team team = gridTilesModel [x, y].Team;
					Piece pieceBelow = grid[x, y + directionalOffset];

					if (pieceBelow.Type == PieceType.EMPTY) {

						piece.MovablePiece.Move(x, y + directionalOffset, FillTime, null);

						grid[x, y + directionalOffset] = piece;

						SetEmpty(x, y);

						movedPiece = true;
					}
					else /*if (GoAround(pieceBelow.Type))*/ {// TODO: 
															 // Try to move diagonaly
						for (int diag = -1; diag <= 1; diag++) {
							if (diag == 0)
								continue;

							int diagX = x + diag;
							if (inverse) diagX = x - diag;

							if (diagX < 0 || diagX >= XDim)
								continue;

							Piece diagonalPiece = grid[diagX, y + directionalOffset];

							if (diagonalPiece.Type == PieceType.EMPTY) {
								bool hasPieceAbove = true;
								// Check for pieces above
								for (int aboveY = y; LoopCondition(team, aboveY); aboveY += directionalOffset) {
									Piece pieceAboveDiagonal = grid[diagX, aboveY];

									if (pieceAboveDiagonal.Type == PieceType.EMPTY)
										continue;
									else if (pieceAboveDiagonal.IsMovable)
										break;
									else if (aboveY == y && GoAround(pieceAboveDiagonal.Type) && GoAround(pieceBelow.Type))
										break;

									hasPieceAbove = false;
								}
								if (!hasPieceAbove) {

									piece.MovablePiece.Move(diagX, y + directionalOffset, FillTime, null);
									grid[diagX, y + directionalOffset] = piece;

									SetEmpty(x, y);

									movedPiece = true;
									break;
								}
							}
						}
					}
				}
			}
		}

		bool addedNew = AddNewPiecesToGrid();
		if (addedNew)
			movedPiece = true;

		//Debug.Log("Moved " + movedPiece);
		return movedPiece;
	}
	private bool AddNewPiecesToGrid() {
		bool addedNew = false;
		for (int x = 0; x < XDim; x++) {
			if (grid[x, YDim - 1].Type == PieceType.EMPTY && FillQues[x].Count > 0) {
				var newPiece = FillQues[x].Dequeue();
				EnableNewQuePiece<GridPiece>(newPiece, x, YDim - 1, x, YDim, PressPiece, ReleasePiece);
				grid[x, YDim - 1].MovablePiece.Move(x, YDim - 1, FillTime, null);
				////Debug.Log("Queue" + x + "has " + FillQues[x].Count + " still in the Queue");
				//Debug.Log("Queue" + x + "has Added type " + newPiece.Type);

				addedNew = true;
			}
		}
		//Debug.Log("Added New To Grid: " + addedNew);
		return addedNew;
	}

	// Swapping
	public bool Swap(Piece piece, Directions direction) {
		bool valid = false;
		Piece otherPiece = GetAdjacent(piece, direction);
		if (otherPiece == null)
			return valid;
		else {
			valid = true;
			SwapPieces(piece, otherPiece);
		}

		return valid;
	}

	private void SwapPieces(Piece piece1, Piece piece2) {
		// Both Movable?
		if (piece1.IsMovable && piece2.IsMovable) {
			int p1x = piece1.X;
			int p1y = piece1.Y;
			int p2x = piece2.X;
			int p2y = piece2.Y;

			// Swap refs in gridPieces so checking is correct
			grid[p1x, p1y] = piece2;
			grid[p2x, p2y] = piece1;

			bool doSwap = false;
			bool p1HasMatch = false;
			bool p2HasMatch = false;
			// Check for match before swapping
			p1HasMatch = CheckMatch(p2x, p2y, piece1.Type);
			p2HasMatch = CheckMatch(p1x, p1y, piece2.Type);

			if (HasToBeMatchToSwap) {
				if (p1HasMatch || p2HasMatch)
					doSwap = true;

				if (!doSwap) {
					piece1.MovablePiece.Move(p2x, p2y, FillTime / 2f, () => { piece1.MovablePiece.Move(p1x, p1y, FillTime / 2f, null); });
					piece2.MovablePiece.Move(p1x, p1y, FillTime / 2f, () => { piece2.MovablePiece.Move(p2x, p2y, FillTime / 2f, null); });
				}
			}
			else
				doSwap = true;// Swap regardless

			if (doSwap) {
				// Swap'em
				piece1.MovablePiece.Move(p2x, p2y, FillTime, () => { OnSwapComplete(p1HasMatch, p1x, p1y, p2HasMatch, p2x, p2y); });
				piece2.MovablePiece.Move(p1x, p1y, FillTime, null);
			}
			else {
				// Return references
				grid[p1x, p1y] = piece1;
				grid[p2x, p2y] = piece2;
			}
		}
	}
	private void OnSwapComplete(bool p1HasMatch, int p1x, int p1y, bool p2HasMatch, int p2x, int p2y) {
		StartCoroutine(SwapComplete(p1HasMatch, p1x, p1y, p2HasMatch, p2x, p2y));
	}

	bool allyPlaced = false;
	PieceType matchType = PieceType.ANY;
	private IEnumerator SwapComplete(bool p1HasMatch, int p1x, int p1y, bool p2HasMatch, int p2x, int p2y) {
		if (p1HasMatch) {
			var matching = GetAllMatched(grid[p2x, p2y]);
			ClearListPieces(matching);
			matchType = matching[0].Type;
			if (matching.Count >= 4) {
				gridDisplay.HighlightMatching(matching, PlaceAlly);
				allyPlaced = false;
				while (!allyPlaced) {
					yield return null;
				}
			}
		}

		if (p2HasMatch) {
			var matching = GetAllMatched(grid[p1x, p1y]);
			ClearListPieces(matching);
			if (matching.Count >= 4) {
				matchType = matching[0].Type;
				gridDisplay.HighlightMatching(matching, PlaceAlly);
				allyPlaced = false;
				while (!allyPlaced) {
					yield return null;
				}
			}
		}

		if (p1HasMatch || p2HasMatch) {
			if (OnSwapMatched != null)
				OnSwapMatched();
			BeginFill();
		}
	}

	private void PlaceAlly(int x, int y) {
		//EnableNewPiece(x, y, x, y, PieceType.ALLY, null, null);
		var piece = EnableNewPoolPiece<GridPiece>(x, y, x, y, PieceType.ALLY, null, null);
		AllyFourPiece ally = piece.GetComponent<AllyFourPiece>();
		if (ally != null) {
			ally.AllyType = matchType;
			ally.AimTurret(() => { allyPlaced = true; });
		}
		else {
			allyPlaced = true;
		}
	}

	

	// Checkers & Validaotrs
	private bool CheckBounds(int x, int y) {
		if (x < 0 || x >= XDim || y < 0 || y >= YDim)
			return false;
		else
			return true;
	}
	private bool CheckMatch(int x, int y, PieceType type) {
		if (QuickMatchCheckHorizontal(x, y, type) || QuickMatchCheckVertical(x, y, type))
			return true;
		else
			return false;
	}
	private bool QuickMatchCheckVertical(int x, int CheckY, PieceType type) {

		int consecutiveCount = 0;

		for (int dir = 0; dir < 2; dir++) {

			for (int yOffset = 1; yOffset < YDim; yOffset++) {
				int y = dir == 0 ? CheckY - yOffset : CheckY + yOffset; // Direction ? down : up

				if (y < 0 || y >= YDim)
					break; // Check Bounds

				if (grid[x, y].Type == type && grid[x, y].IsMovable)
					consecutiveCount++;
				else
					break;
			}
			if (consecutiveCount >= 2)
				return true;
		}
		return false;
	}
	private bool QuickMatchCheckHorizontal(int CheckX, int y, PieceType type) {

		int consecutiveCount = 0;

		for (int dir = 0; dir < 2; dir++) {

			for (int xOffset = 1; xOffset < XDim; xOffset++) {
				int x = dir == 0 ? CheckX - xOffset : CheckX + xOffset; // Direction ? left : right

				if (x < 0 || x >= XDim)
					break; // Check Bounds

				if (grid[x, y].Type == type && grid[x, y].IsMovable)
					consecutiveCount++;
				else
					break;
			}
			if (consecutiveCount >= 2)
				return true;
		}
		return false;
	}


	// Getters
	public Piece GetPiece(int x, int y) {
		if (CheckBounds(x, y))
			return grid[x, y];
		else return null;
	}
	public static Vector2 GetGridWorldPosition(int x, int y) {
		return Instance.GetWorldPosition(x, y);
	}

	List<Piece> matchingPieces = new List<Piece> ();
	List<Piece> matchingHorizontalPieces = new List<Piece> ();
	List<Piece> matchingVerticalPieces = new List<Piece> ();
	private List<Piece> GetAllMatched(Piece piece) {
		// Add piece cos its in da mach yo
		matchingPieces.Clear();
		matchingHorizontalPieces.Clear();
		matchingVerticalPieces.Clear();

		int x = piece.X;
		int y = piece.Y;
		// Horizontal First
		matchingHorizontalPieces.Add(piece);
		for (int dir = 0; dir < 2; dir++) {

			for (int i = 0; i < XDim; i++) {
				x = dir == 0 ? x -= 1 : x += 1; // Direction ? left : right

				if (x < 0 || x >= XDim)
					break; // Check Bounds

				if (grid[x, y].Type == piece.Type)
					matchingHorizontalPieces.Add(grid[x, y]);
				else
					break;
			}
			x = piece.X;
		}
		if (matchingHorizontalPieces.Count >= 3) {
			// Add successful pices to matching list
			for (int i = 0; i < matchingHorizontalPieces.Count; i++) {
				if (!matchingPieces.Contains(matchingHorizontalPieces[i]))
					matchingPieces.Add(matchingHorizontalPieces[i]);
			}

			// Check matching pieces for Vertical matches
			for (int j = 0; j < matchingHorizontalPieces.Count; j++) {
				x = matchingHorizontalPieces[j].X;
				y = matchingHorizontalPieces[j].Y;

				for (int dir = 0; dir < 2; dir++) {

					for (int i = 0; i < YDim; i++) {
						y = dir == 0 ? y -= 1 : y += 1; // Direction ? down : up

						if (y < 0 || y >= YDim)
							break; // Check Bounds

						if (grid[x, y].Type == piece.Type)
							matchingVerticalPieces.Add(grid[x, y]);
						else
							break;
					}
					y = piece.Y;
				}
				//if (matchingVerticalPieces.Count >= 2) {
				for (int i = 0; i < matchingVerticalPieces.Count; i++) {
					if (!matchingPieces.Contains(matchingVerticalPieces[i]))
						matchingPieces.Add(matchingVerticalPieces[i]);
				}
				//break;
				//}
				//else {
				matchingVerticalPieces.Clear();
				//}
			}

			// Found our match so return
			return matchingPieces;

		}

		// Reset For Vertical
		matchingHorizontalPieces.Clear();
		matchingVerticalPieces.Clear();
		x = piece.X;
		y = piece.Y;

		// Vertical
		matchingVerticalPieces.Add(piece);
		for (int dir = 0; dir < 2; dir++) {

			for (int i = 1; i < YDim; i++) {
				y = dir == 0 ? y -= 1 : y += 1; // Direction ? down : up

				if (y < 0 || y >= YDim)
					break; // Check Bounds

				if (grid[x, y].Type == piece.Type)
					matchingVerticalPieces.Add(grid[x, y]);
				else
					break;
			}
			y = piece.Y;
		}
		if (matchingVerticalPieces.Count >= 3) {
			for (int i = 0; i < matchingVerticalPieces.Count; i++) {
				if (!matchingPieces.Contains(matchingVerticalPieces[i]))
					matchingPieces.Add(matchingVerticalPieces[i]);
			}
			////Check this match for Vertical Matches
			for (int j = 0; j < matchingVerticalPieces.Count; j++) {
				x = matchingVerticalPieces[j].X;
				y = matchingVerticalPieces[j].Y;

				for (int dir = 0; dir < 2; dir++) {

					for (int i = 0; i < YDim; i++) {
						x = dir == 0 ? x -= 1 : x += 1; // Direction ? left : rigth

						if (x < 0 || x >= XDim)
							break; // Check Bounds

						if (grid[x, y].Type == piece.Type)
							matchingHorizontalPieces.Add(grid[x, y]);
						else
							break;
					}
					x = piece.X;
				}
				//if (matchingHorizontalPieces.Count >= 2) {
				for (int i = 0; i < matchingHorizontalPieces.Count; i++) {
					if (!matchingPieces.Contains(matchingHorizontalPieces[i]))
						matchingPieces.Add(matchingHorizontalPieces[i]);
				}
				//break;
				//	}
				//else {
				matchingHorizontalPieces.Clear();
				//}
			}

			// Found our match so returnd
			return matchingPieces;
		}

		if (matchingPieces.Count < 3) {
			matchingPieces.Clear();
		}

		return matchingPieces;
	}
	private Piece GetAdjacent(Piece piece, Directions direction) {
		int x = piece.X;
		int y = piece.Y;
		switch (direction) {
			case Directions.UP:
				if (FillDirection == Directions.UP) {
					if (CheckBounds(x, y - 1))
						return grid[x, y - 1];
				}
				else {
					if (CheckBounds(x, y + 1))
						return grid[x, y + 1];
				}
				break;

			case Directions.DOWN:
				if (FillDirection == Directions.UP) {
					if (CheckBounds(x, y + 1))
						return grid[x, y + 1];
				}
				else {
					if (CheckBounds(x, y - 1))
						return grid[x, y - 1];
				}
				break;

			case Directions.LEFT:
				if (CheckBounds(x - 1, y))
					return grid[x - 1, y];
				break;

			case Directions.RIGHT:
				if (CheckBounds(x + 1, y))
					return grid[x + 1, y];
				break;
		}

		return null;
	}
	private Vector2 GetWorldPosition(int x, int y) {
		Vector3 position;
		if(FillDirection == Directions.UP)
			position =  new Vector2((transform.position.x - (XDim / 2.0f) + x + 0.5f) * PositionScalar, (transform.position.y + (YDim / 2.0f) - y - 0.5f + HeightOffset) * PositionScalar);
		else
			position = new Vector2((transform.position.x - (XDim / 2.0f) + x + 0.5f) * PositionScalar, (transform.position.y - (YDim / 2.0f) + y + 0.5f + HeightOffset) * PositionScalar);
		return position;
	}

	Piece[] adjacent = new Piece [4];
	private Piece [] GetAdjacent(int x, int y) {
		for (int i = 0; i < 4; i++) {
			adjacent[i] = null;
		}
		if (CheckBounds(x, y + 1)) { // UP
			if (grid[x, y + 1].Type == PieceType.OBSTACLE) //TODO: || other proximity clear types
				adjacent[0] = grid[x, y + 1];
		}
		if (CheckBounds(x, y - 1)) {// Down
			if (grid[x, y - 1].Type == PieceType.OBSTACLE)
				adjacent[1] = grid[x, y - 1];
		}
		if (CheckBounds(x - 1, y)) {// Left
			if (grid[x - 1, y].Type == PieceType.OBSTACLE)
				adjacent[2] = grid[x - 1, y];
		}
		if (CheckBounds(x + 1, y)) {// Right
			if (grid[x + 1, y].Type == PieceType.OBSTACLE)
				adjacent[3] = grid[x + 1, y];
		}
		return adjacent;
	}
	public static bool GoAround(PieceType type) {
		return type == PieceType.OBSTACLE || type == PieceType.ALLY;
	}

	// Activators
	private Piece EnableNewPoolPiece<T> (int gridX, int gridY, int posX, int posY, PieceType type, Action<GridPiece> mouseDown, Action<Directions> mouseUp) where T : Piece{
		Piece piece = null;
		if (typeof (T) == typeof (GridPiece))
		{
			GridPiece newPiece = piecePools [type].GetNewObject ().GetComponent<GridPiece> ();

			if (newPiece == null)
			{
				//Debug.LogError(string.Format("{0} has no available pieces", type.ToString()));
				return null;
			}

			if (newPiece.Type == PieceType.ALLY && OnAllyActorCreated != null)
				OnAllyActorCreated (newPiece.GetComponent<Actor> ());

			grid [gridX, gridY] = newPiece;
			newPiece.Set (posX, posY, mouseDown, mouseUp);
			piece = newPiece;
		}

		return piece;
	}
	private void EnableNewQuePiece<T>(Piece newPiece, int gridX, int gridY, int posX, int posY, Action<GridPiece> mouseDown, Action<Directions> mouseUp) where T : Piece{
		if (newPiece.Type == PieceType.ALLY && OnAllyActorCreated != null)
			OnAllyActorCreated(newPiece.GetComponent<Actor>());

		if (typeof (T) == typeof (GridPiece))
		{
			GridPiece gridPiece = newPiece.GetComponent<GridPiece> ();
			gridPiece.Set (posX, posY, mouseDown, mouseUp);
		}

		grid [gridX, gridY] = newPiece;

		if (newPiece.IsMovable)
			newPiece.MovablePiece.Move(gridX, gridY, FillTime, null);
	}
	private GridPiece EnableNewPieceLite(PieceType type) {
		return piecePools[type].GetNewObject();
	}

	// Deactivators
	public void ClearListPieces(List<Piece> list, float timeStep) {// For Lightning power
		StartCoroutine(DoClearListPieces(list, timeStep));
	}
	public void ClearListPieces(List<Piece> list) {
		for (int i = 0; i < list.Count; i++) {
			//if (list[i].ClearablePiece.Clear()) {
			//	//Debug.Log(string.Format("GRID CLEARING {0}:{1}, {2}", list[i].X, list[i].Y, list[i].Type));
			//	SetEmpty(list[i].X, list[i].Y);
			//}
		}
	}
	public void ClearPiece(GridPiece piece) {
		//if (piece.ClearablePiece.Clear())
		//	SetEmpty(piece.X, piece.Y);
	}

	private void SetEmpty(int x, int y) {
		grid[x, y] = emptyPiece;
	}

	List<Piece> PiecesToClear = new List<Piece>();
	private bool ClearAllMatches() {
		//Debug.Log("Clear All Matches");
		bool needsFill = false;

		if (PiecesToClear.Count > 0)
			PiecesToClear.Clear();

		int checkOffset = 3;
		int xCheck = 2;
		int yCheck = 2;

		for (int y = 0; y < YDim; y++) {
			for (int x = 0; x < XDim; x++) {

				if (x == xCheck && grid[x, y].IsMatchable) {
					List<Piece> matching = GetAllMatched(grid[x,y]);
					if (matching.Count >= 3) {
						if (matching.Count >= 4) {
							//TODO: do Spawn Ally Sunroutine
						}
						for (int j = 0; j < matching.Count; j++) {
							if (!PiecesToClear.Contains(matching[j]))
								PiecesToClear.Add(matching[j]);

							var adjacentObstacles = GetAdjacent(x,y);
							for (int k = 0; k < adjacentObstacles.Length; k++) {
								if (adjacent[k] != null && !PiecesToClear.Contains(adjacentObstacles[k]))
									PiecesToClear.Add(adjacentObstacles[k]);
							}
						}
					}
					xCheck += checkOffset;
				}

				if (y == yCheck && grid[x, y].IsMatchable) {

					List<Piece> matching = GetAllMatched(grid[x,y]);
					if (matching.Count >= 3) {
						if (matching.Count >= 4) {
							//TODO: do Spawn Ally Sunroutine
						}
						for (int j = 0; j < matching.Count; j++) {
							if (!PiecesToClear.Contains(matching[j]))
								PiecesToClear.Add(matching[j]);

							var adjacentObstacles = GetAdjacent(x,y);
							for (int k = 0; k < adjacentObstacles.Length; k++) {
								if (adjacent[k] != null && !PiecesToClear.Contains(adjacentObstacles[k]))
									PiecesToClear.Add(adjacentObstacles[k]);
							}
						}
					}
				}
			}
			xCheck = 2;

			if (y == yCheck)
				yCheck += checkOffset;
		}
		if (PiecesToClear.Count >= 3) {
			ClearListPieces(PiecesToClear);
			needsFill = true;
		}

		//Debug.Log("Need Fill " + needsFill);
		return needsFill;
	}
	private IEnumerator DoClearListPieces(List<Piece> list, float timestep) {
		for (int i = 0; i < list.Count; i++) {
			//if (list[i].ClearablePiece.Clear()) {
			//	SetEmpty(list[i].X, list[i].Y);
			//}
			yield return new WaitForSeconds(timestep);
		}
		BeginFill();
	}

}
