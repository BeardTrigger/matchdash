﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileColor {
	public PieceType Type;
	public Color ActiveColor = Color.white;
	public Color HighlightedColor = Color.white;
	public Color SetColor = Color.white;
}

public class GridDisplay : MonoBehaviour {

	public SpriteRenderer FillingLockScreenSprite;
	[Space]

	public GameObject PiecesParent;
	private Collider2D[] PiecesColliders;

	public TileColor[] TileColors;
	private Dictionary<PieceType, TileColor> ColorDict;

	private GridTileView[,] Tiles;

	List<Piece> currentMatching;

	private void Awake() {
		ColorDict = new Dictionary<PieceType, TileColor>();
		for (int i = 0; i < TileColors.Length; i++) {
			if (!ColorDict.ContainsKey(TileColors[i].Type))
				ColorDict.Add(TileColors[i].Type, TileColors[i]);
		}
		PiecesColliders = PiecesParent.GetComponentsInChildren<Collider2D>();
	}

	public void Initialize(GridTileView[,] tiles) {
		Tiles = tiles;
	}

	public void HighlightMatching(List<Piece> matching, Action<int, int> OnSelected) {
		currentMatching = matching;
		TileColor color = ColorDict[matching[0].Type];
		for (int i = 0; i < matching.Count; i++) {
			Tiles[matching[i].X, matching[i].Y].ApplyColor(color, OnSelected, BackToNeutralMatch);
		}
		SetCollidersActiveState(false);
	}

	public void BackToNeutralMatch() {
		SetCollidersActiveState(true);
		if (currentMatching != null) {
			for (int i = 0; i < currentMatching.Count; i++) {
				Tiles[currentMatching[i].X, currentMatching[i].Y].ToNeutral();
			}
			currentMatching = null;
		}
	}


	public void Highlight(List<GridTileView> highlight, PieceType type) {
		TileColor color = ColorDict[type];

		for (int i = 0; i < highlight.Count; i++) {
			highlight[i].ApplyColor(color);
		}
		SetCollidersActiveState(false);
	}

	public void SetTileColor(List<GridTileView> SetTiles, PieceType type) {
		TileColor color = ColorDict[type];
		for (int i = 0; i < SetTiles.Count; i++) {
			SetTiles[i].SetTileColor(color);
		}
		SetCollidersActiveState(true);
	}

	public void BackToNeutralHighlight(List<GridTileView> highlight, bool clear = false) {
		if (highlight != null) {
			for (int i = 0; i < highlight.Count; i++) {
				highlight[i].ToNeutral(clear);
			}
		}
	}

	private void SetCollidersActiveState(bool active) {
		for (int i = 0; i < PiecesColliders.Length; i++) {
			PiecesColliders[i].enabled = active;
		}
	}

	public void SetFillingLockDispayState (bool state)
	{
		FillingLockScreenSprite.enabled = state;
	}
}
