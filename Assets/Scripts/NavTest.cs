﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavTest : MonoBehaviour {

	public Transform Victim;

	Vector3 position;

	private void Awake ()
	{
		position = transform.position;
	}

	// Use this for initialization
	void Start () {
		GetComponent<NavMeshAgent> ().SetDestination (Victim.position);
	}
	
	// Update is called once per frame
	void Update () {
		if ((transform.position - Victim.position).magnitude < 2)
			transform.position = position;
	}
}
