﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class SceneSelectMenu {

	[MenuItem("Scenes/Menu")]
	private static void OpenMenuScene() {
		var loaded = EditorSceneManager.GetActiveScene();
		if (!loaded.name.Equals("Menu")) {
			EditorSceneManager.OpenScene("Assets/Scenes/Menu.unity");
		}
	}

	[MenuItem("Scenes/Game")]
	private static void OpenGameScene() {
		var loaded = EditorSceneManager.GetActiveScene();
		if (!loaded.name.Equals("Game")) {
			EditorSceneManager.OpenScene("Assets/Scenes/Game.unity");
		}
	}

	[MenuItem ("Scenes/Two Player Game")]
	private static void OpenTwoPlayerGameScene ()
	{
		var loaded = EditorSceneManager.GetActiveScene ();
		if (!loaded.name.Equals ("TwoPlayer"))
		{
			EditorSceneManager.OpenScene ("Assets/Scenes/TwoPlayer.unity");
		}
	}
}
