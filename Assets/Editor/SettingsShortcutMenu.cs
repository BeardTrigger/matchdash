﻿using UnityEditor;
using UnityEngine;

public class SettingsShortcutMenu {

	[MenuItem("Settings/Player", false, 1)]
	private static void OpenPlayerSettings() {
		EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
	}

	[MenuItem("Settings/Quality", false, 2)]
	private static void OpenQualitySettings() {
		EditorApplication.ExecuteMenuItem("Edit/Project Settings/Quality");
	}

	[MenuItem ("Settings/Graphics", false, 2)]
	private static void OpenGraphicsSettings ()
	{
		EditorApplication.ExecuteMenuItem ("Edit/Project Settings/Graphics");
	}
}
